/*
	Author: 		Pranab Sinha, Shreelaxmi
	Created Date: 	10th September 2019
	Modified Date: 	19th September 2019
	Description: 	All the functions useful for admin
 */


package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class Admin {

	private JSONObject JSONRequestMessageObject = null;
	private String username = null;
	private String password = null;

	public Admin(JSONObject JSONRequestMessage) {
		this.JSONRequestMessageObject = JSONRequestMessage;
	}

	@SuppressWarnings("unchecked")
	public JSONObject AddUser() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		boolean successMessage = true;
		String message = "";
		try {
			username = (String) this.JSONRequestMessageObject.get("UserName");
			password = (String) this.JSONRequestMessageObject.get("Password");
			if(password.isEmpty() || password.isBlank() || username.isEmpty() || username.isBlank()) {
				throw new Exception("Username or password is empty or blank, provide proper value");
			}
			String passwordEncoded = Base64.getEncoder().encodeToString(password.getBytes());
			String checkunamequery = "SELECT ID FROM ADMIN_USER WHERE USERNAME = '" + username + "';";
			logger.info(checkunamequery);

			ResultSet rs = DBConnect.selectData(con, checkunamequery);

			if (rs.next() == false) {

				String updateSQL = "INSERT INTO ADMIN_USER (`USERNAME`, `PASSWORD`) VALUES ('" + username + "', '" + passwordEncoded + "');";
				logger.info(updateSQL);
				int res = DBConnect.updateData(con, updateSQL);
				if (res == -1) {
					throw new Exception("INSERT Query Failed: " + updateSQL);
				} else {
					logger.info("User Added Successful: " + username);
					message = "User added successfully";
				}
			}
			else {
				successMessage = false;
				logger.info("User already added: " + username);
				message = "User already added";
			}
		} catch (Exception e) {
			successMessage = false;
			message = "Some error occured - " + e;
			logger.error(e);

		}

		responseParameterList.put("UserName", username);
		responseParameterList.put("Status", message);
		returnResponseObject.put("Success", successMessage);
		returnResponseObject.put("ParameterList", responseParameterList);

		con.close();

		return returnResponseObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject Login() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		String message = null;
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		boolean statusMessage = true;

		try {

			username = (String) this.JSONRequestMessageObject.get("UserName");
			password = (String) this.JSONRequestMessageObject.get("Password");
			if(password.isEmpty() || password.isBlank() || username.isEmpty() || username.isBlank()) {
				throw new Exception("Username or password is empty, provide proper value");
			}
			String passwordHash = Base64.getEncoder().encodeToString(password.getBytes());
			String userNameInDB = null;
			String databasePassword = null;
			String sqlQueryUserName = "SELECT USERNAME FROM ADMIN_USER WHERE USERNAME = '" + username + "';";
			logger.info(sqlQueryUserName);
			ResultSet rs = DBConnect.selectData(con, sqlQueryUserName);

			while (rs.next()) {

				userNameInDB = rs.getString(1);

				String sqlQueryGetDBPassword = "SELECT PASSWORD FROM ADMIN_USER WHERE USERNAME = '" + userNameInDB + "';";
				ResultSet rsp = DBConnect.selectData(con, sqlQueryGetDBPassword);

				while(rsp.next()) {
					databasePassword = rsp.getString(1);
					if(databasePassword.equals(passwordHash)) {
						message = "Login Successful";
						logger.info(message);
					}
					else {
						message = "Password doesn't match for this user - " + userNameInDB;
						logger.info(message);
					}
				}
				if (databasePassword == null || databasePassword == "0") {

					message = "Error while fetching password from database - " + databasePassword;
					throw new Exception(message);
				}

			}

			if (userNameInDB == null || userNameInDB == "0") {

				message = "UserName not found in the database - " + username;
				throw new Exception(message);
			}

			con.close();

		} catch (Exception e) {

			statusMessage = false;
			message = "Error while logging - " + e;
			con.close();
			logger.error(e);
		}

		responseParameterList.put("Status", message);
		responseParameterList.put("UserName", username);

		returnResponseObject.put("ParameterList", responseParameterList);
		returnResponseObject.put("Success", statusMessage);

		return returnResponseObject;

	}

	@SuppressWarnings("unchecked")
	public JSONObject UpdateUser() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		String message = null;
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		boolean statusMessage = true;

		try {
			username = (String) this.JSONRequestMessageObject.get("UserName");
			String newPassword = (String) this.JSONRequestMessageObject.get("NewPassword");
			String oldPassword = (String) this.JSONRequestMessageObject.get("OldPassword");
			if(username.isEmpty() || username.isBlank()) {
				throw new Exception("Username is empty, provide proper value");
			}
			if(oldPassword.isEmpty() || oldPassword.isBlank()) {
				throw new Exception("Old Password is empty, provide proper value");
			}
			if(newPassword.isEmpty() || newPassword.isBlank()) {
				throw new Exception("New Password is empty, provide proper value");
			}

			String oldPasswordHash = Base64.getEncoder().encodeToString(oldPassword.getBytes());
			String newPasswordHash = Base64.getEncoder().encodeToString(newPassword.getBytes());
			String userNameInDB = null;
			String databasePassword = null;
			String sqlQueryUserName = "SELECT USERNAME FROM ADMIN_USER WHERE USERNAME = '" + username + "';";
			logger.info(sqlQueryUserName);
			ResultSet rs = DBConnect.selectData(con, sqlQueryUserName);

			while (rs.next()) {

				userNameInDB = rs.getString(1);

				if (userNameInDB == null || userNameInDB == "0") {

					message = "UserName not found in the database - " + username;
					throw new Exception(message);
				}

				String sqlQueryGetDBPassword = "SELECT PASSWORD FROM ADMIN_USER WHERE USERNAME = '" + userNameInDB + "';";
				ResultSet rsp = DBConnect.selectData(con, sqlQueryGetDBPassword);

				while(rsp.next()) {
					databasePassword = rsp.getString(1);
					if (databasePassword == null || databasePassword == "0") {

						message = "Error while fetching password from database - " + databasePassword;
						throw new Exception(message);
					}
					if(databasePassword.equals(oldPasswordHash)) {
						String updatePasswordQuery = "UPDATE ADMIN_USER SET PASSWORD = '" + newPasswordHash + "' WHERE "
								+ "USERNAME = '" + userNameInDB + "'";
						int result = DBConnect.updateData(con, updatePasswordQuery);

						if(result == -1) {
							message = "Password Update Failed";
							logger.info(message);
							throw new Exception(message);
						}
						else {
							message = "Password Update Success";
							logger.info(message);
						}

					}
					else {
						message = "Existing password doesn't match for this user - " + userNameInDB;
						logger.info(message);
						throw new Exception(message);
					}
				}


			}

			con.close();

		} catch (Exception e) {
			statusMessage = false;
			message = "Error while updating password - " + e;
			con.close();
			logger.error(e);
		}

		responseParameterList.put("Status", message);
		responseParameterList.put("UserName", username);

		returnResponseObject.put("ParameterList", responseParameterList);
		returnResponseObject.put("Success", statusMessage);

		return returnResponseObject;

	}

	@SuppressWarnings("unchecked")
	public JSONObject addQuestion() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		String message = null;
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		boolean statusMessage = true;

		try {
			HashMap<String, String> optionsValue = new HashMap<String, String>();
			String questionStr = (String) this.JSONRequestMessageObject.get("Question");
			logger.info(questionStr);
			String moduleId = (String) this.JSONRequestMessageObject.get("ModuleId");
			logger.info(moduleId);
			String correctAnswer = (String) this.JSONRequestMessageObject.get("Answer");
			logger.info(correctAnswer);
			JSONArray questionOptions = (JSONArray) this.JSONRequestMessageObject.get("QuestionOptions");
			logger.info(questionStr + " " + moduleId + " " + correctAnswer);
			int arrayLength = questionOptions.size();
			for(int i = 0; i < arrayLength; i++) {
				JSONObject options = (JSONObject) questionOptions.get(i);
				optionsValue.put((String)options.get("OptionId"), (String) options.get("OptionValue"));
				logger.info((String)options.get("OptionId") + " " + (String) options.get("OptionValue"));
			}
			String insertQuestions = "INSERT INTO QUESTIONS (QUESTION, OPTION1, OPTION2, OPTION3,"
					+ " OPTION4, ANSWER, MODULE_ID) VALUES('" + questionStr + "', '" + optionsValue.get("1") + "'"
					+ ", '" + optionsValue.get("2") + "', '" + optionsValue.get("3") + "', '" + optionsValue.get("4") + "', '"
					+ correctAnswer	+ "', '" + moduleId + "');";

			logger.info("Query to add question - " + insertQuestions);
			int insertStatus = DBConnect.updateData(con, insertQuestions);

			if(insertStatus == -1) {
				throw new Error("Please send proper data for adding question.");
			}
			else {
				message = "Question added successfully.";
			}

		}
		catch(Exception e) {
			statusMessage = false;
			message = "Error while adding question - " + e;
			con.close();
			logger.error(e);
		}

		responseParameterList.put("Status", message);

		returnResponseObject.put("ParameterList", responseParameterList);
		returnResponseObject.put("Success", statusMessage);

		return returnResponseObject;
	}
	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject setCollegeStatus() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		String dropdownFieldValue = "";
		String dropDownKeyNameList = "";

		try {

			dropdownFieldValue = (String) this.JSONRequestMessageObject.get("CollegeId");

			String updateQuery = null;

			// Check condition and fetch the data and keyName
			if (dropdownFieldValue != null) {

				dropDownKeyNameList = "CollegeList";
				updateQuery = "UPDATE COLLEGE_DETAILS SET " + 
						"CURRENT_STATUS = CASE WHEN ID =" + dropdownFieldValue + " THEN 1 ELSE 0 END;" ;
			}else {

				throw new Exception("No mapping found for " + dropdownFieldValue);
			}
			logger.info(updateQuery);

			int status = DBConnect.updateData(con, updateQuery);

			if(status == -1) {
				throw new Exception("Error while updating college records.");
			}

			JSONArray dropdownValuesFromDB = new JSONArray();

			responseParameterList.put("UIDropdownField", dropdownFieldValue);
			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.info(returnResponseObject.toString());

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("Reason", e.getMessage());
			responseParameterList.put("UIDropdownField", dropdownFieldValue);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}

	}

	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject getAllStatus() throws Exception {
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ResultSet rs = null;

		String dropdownFieldValue = "";
		String dropDownKeyNameList = "";

		try {
			dropdownFieldValue = (String) this.JSONRequestMessageObject.get("UIDropdownField");
			String selectQuery = null;

			// Check condition and fetch the data and keyName
			if (dropdownFieldValue.equals("College")) {
				dropDownKeyNameList = "CollegeList";
				selectQuery = "SELECT * FROM COLLEGE_DETAILS;";
			} else if (dropdownFieldValue.equals("Module")) {
				dropDownKeyNameList = "ModuleList";
				selectQuery = "SELECT * FROM MODULES;";
			}else if (dropdownFieldValue.equals("Degree")) {
				dropDownKeyNameList = "DegreeList";
				selectQuery = "SELECT * FROM DEGREE;";
			}else if (dropdownFieldValue.equals("Branch")) {
				dropDownKeyNameList = "DegreeList";
				selectQuery = "SELECT * FROM BRANCH;";
			}else {
				throw new Exception("No mapping found for " + dropdownFieldValue);
			}
			logger.info(selectQuery);

			rs = DBConnect.selectData(con, selectQuery);
			JSONArray dropdownValuesFromDB = new JSONArray();

			while (rs.next()) {
				JSONObject eachData = new JSONObject();
				eachData.put("Id", rs.getInt(1));
				eachData.put("Name", rs.getString(2));
				dropdownValuesFromDB.add(eachData);
			}

			if (dropdownValuesFromDB == null) {
				throw new Exception("No Data found for " + dropDownKeyNameList);
			}

			responseParameterList.put(dropDownKeyNameList, dropdownValuesFromDB);
			responseParameterList.put("UIDropdownField", dropdownFieldValue);
			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.info(returnResponseObject.toString());

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("Reason", e.getMessage());
			responseParameterList.put("UIDropdownField", dropdownFieldValue);
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}

	}

	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject getMandatoryModules() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ResultSet rs = null;

		String dropdownFieldValue = "";
		String dropDownKeyNameList = "";

		try {
			dropdownFieldValue = (String) this.JSONRequestMessageObject.get("UIDropdownFieldValue");

			dropDownKeyNameList = "ModuleList";
			String selectQuery = "SELECT * FROM MODULES WHERE MANDATORY_MODULE = 1;";

			// query to fetch the details from the database
			logger.info(selectQuery);

			rs = DBConnect.selectData(con, selectQuery);

			JSONArray dropdownValuesFromDB = new JSONArray();
						
			int id = 0;
			while (rs.next()) {
				
				JSONObject eachData1 = new JSONObject();
				eachData1.put("moduleName", rs.getString(2));
				dropdownValuesFromDB.add(eachData1);

			}
			JSONObject eachData2 = new JSONObject();
			eachData2.put("moduleName", "Coding");
			dropdownValuesFromDB.add(eachData2);
			JSONObject eachData3 = new JSONObject();
			eachData3.put("moduleName", "Score");
			dropdownValuesFromDB.add(eachData3);
			JSONObject eachData4 = new JSONObject();
			eachData4.put("moduleName", "Total");
			dropdownValuesFromDB.add(eachData4);
			JSONObject eachData5 = new JSONObject();
			eachData5.put("moduleName", "OptionalModule");
			dropdownValuesFromDB.add(eachData5);
			
			if (dropdownValuesFromDB == null) {

				throw new Exception("No Data found for " + dropDownKeyNameList);
			}

			responseParameterList.put(dropDownKeyNameList, dropdownValuesFromDB);
			responseParameterList.put("UIDropdownFieldValue", dropdownFieldValue);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.info(returnResponseObject.toString());

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("Reason", e.getMessage());
			responseParameterList.put("UIDropdownFieldValue", dropdownFieldValue);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}


	}


}
