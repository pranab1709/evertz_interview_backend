/*
	Author: 		Pranab Sinha, Sripad, Shreelaxmi
	Created Date: 	10th September 2019
	Modified Date: 	19th September 2019
	Description: 	All the functions to Save, Get, Delete and Update Candidate information is available in this class
 */

package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class Candidate {

	private JSONObject candidateDetailsJSON;

	String registerNumber = "";
	String studentName = "";
	String dob = "";
	String collegeId = "";
	String genderString = "";
	String degreeId = "";
	String streamId = "";
	String yearOfPassing = "";
	String cgpa = "";
	String email = "";
	String mobile = "";
	String languageSelected = "";
	String response = "";
	String questionType = "";

	public Candidate(JSONObject candidateDetailsJSON) {

		this.candidateDetailsJSON = candidateDetailsJSON;
	}

	@SuppressWarnings({ "unchecked" })
	public JSONObject register() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		String registerNumberFromDB = null;
		String gender = null;

		ResultSet rs = null;

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			registerNumber = (String) this.candidateDetailsJSON.get("RegisterNumber");
			studentName = (String) this.candidateDetailsJSON.get("StudentName");
			dob = (String) this.candidateDetailsJSON.get("DOB");
			collegeId = (String) this.candidateDetailsJSON.get("College");
			genderString = (String) this.candidateDetailsJSON.get("Gender");
			degreeId = (String) this.candidateDetailsJSON.get("Degree");
			streamId = (String) this.candidateDetailsJSON.get("Stream");
			yearOfPassing = (String) this.candidateDetailsJSON.get("YearOfPassing");
			cgpa = (String) this.candidateDetailsJSON.get("CGPA");
			email = (String) this.candidateDetailsJSON.get("Email");
			mobile = (String) this.candidateDetailsJSON.get("Mobile");
			languageSelected = (String) this.candidateDetailsJSON.get("LanguageSelected");

			// Handling Gender to send bit to DB			
			if (genderString == "Male") {
				gender = "b'1'";
			} else {

				gender = "b'0'";
			}

			UtilityFunctions utilityFunction = new UtilityFunctions();
			utilityFunction.validateRegisterNumber(registerNumber);
			utilityFunction.validateCandidatetName(studentName);
			utilityFunction.validateDateOfBirth(dob);
			utilityFunction.validateEmailId(email);
			utilityFunction.validateMobileNumber(mobile);
			utilityFunction.validateCgpa(cgpa);
			utilityFunction.validateGraduationYear(yearOfPassing);

			String checkRegisterNumberQuery = "SELECT REG_NO FROM CANDIDATE_DETAILS WHERE REG_NO = '" + registerNumber + "';";
			logger.info(checkRegisterNumberQuery);

			rs = DBConnect.selectData(con, checkRegisterNumberQuery);

			while (rs.next()) {
				registerNumberFromDB = rs.getString(1);

				if (registerNumberFromDB.equals(registerNumber)) {

					throw new Exception("Register number already exists. Please contact Administrator to reset.");
				}
			}

			// Query string to insert registration details
			String updateSQL = "INSERT INTO CANDIDATE_DETAILS ( `REG_NO`, `NAME`, `DOB`, `GENDER`, `EMAIL`, `MOB_NO`, `COLLEGE_DETAILS_ID`, `DEGREE_ID`, `BRANCH_ID`, `GRAD_YEAR`, `CGPA`, `MODULE_ID`) VALUES ( '"
					+ registerNumber + "', '" + studentName + "', '" + dob + "', " + gender + ", '" + email + "', '"
					+ mobile + "', '" + collegeId + "', '" + degreeId + "', '" + streamId + "', '" + yearOfPassing
					+ "', '" + cgpa + "', '" + languageSelected + "');";

			logger.info(updateSQL);

			int res = DBConnect.updateData(con, updateSQL);

			if (res == -1) {

				throw new Exception("INSERT Query Failed: " + updateSQL);
			} else {

				logger.info("Registration successful: " + registerNumber);
			}

			responseParameterList.put("RegisterNumber", registerNumber);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();

			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("RegisterNumber", registerNumber);

			returnResponseObject.put("Success", false);
			responseParameterList.put("Reason", e.getMessage());
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			return returnResponseObject;
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject resetCandidateData() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		boolean statusMessage = true;
		registerNumber = (String) this.candidateDetailsJSON.get("RegisterNumber");
		questionType = (String) this.candidateDetailsJSON.get("QuestionType");
		String candidateDetailsId = null;
		String getIDQuery = "SELECT ID FROM CANDIDATE_DETAILS WHERE REG_NO = '" + registerNumber + "';";
		logger.info(getIDQuery);

		ResultSet rs = DBConnect.selectData(con, getIDQuery);

		while(rs.next()) {
			candidateDetailsId = rs.getString(1);
		}

		String message = "";
		String deleteAllResponseMCQ = "DELETE FROM CANDIDATE_RESPONSE WHERE CANDIDATE_DETAILS_ID = '" + candidateDetailsId + "';";
		String deleteAllResponseCoding = "DELETE FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = '" + candidateDetailsId + "';";
		String deleteUser = "DELETE FROM CANDIDATE_DETAILS WHERE ID = '" + candidateDetailsId + "';";

		try {

			//Deleting all details of candidate
			if(questionType.equals("All")) {

				//Deleting MCQ Questions
				int deleteStatusMCQ = DBConnect.updateData(con, deleteAllResponseMCQ);

				if (deleteStatusMCQ == -1) {

					throw new Exception("Error deleting records in CANDIDATE_RESPONSE for Register Number: "
							+ registerNumber);
				}

				logger.info("Deletion successful in CANDIDATE_RESPONSE for Register Number: " + registerNumber);

				//Deleting Coding Questions
				int deleteStatusCoding = DBConnect.updateData(con, deleteAllResponseCoding);

				if (deleteStatusCoding == -1) {

					throw new Exception("Error deleting records in CANDIDATE_CODING_RESPONSE for Register Number: "
							+ registerNumber);
				}

				logger.info("Deletion successful in CANDIDATE_CODING_RESPONSE for Register Number: " + registerNumber);

				//Deleting User
				int deleteStatusUser = DBConnect.updateData(con, deleteUser);

				if (deleteStatusUser == -1) {

					throw new Exception("Error deleting records in CANDIDATE_DETAILS for Register Number: "
							+ registerNumber);
				}

				logger.info("Deletion successful in CANDIDATE_DETAILS for Register Number: " + registerNumber);

				message = "Reset Successful for all user details";
			}

			//Deleting MCQ Questions
			else if(questionType.equals("MCQ")) {

				//int deleteStatusMCQ = DBConnect.updateData(con, deleteAllResponseMCQ);
//
//				if (deleteStatusMCQ == -1) {
//
//					throw new Exception("Error deleting records in CANDIDATE_RESPONSE for Register Number: "
//							+ registerNumber);
//				}

				String updateCandidateDetails = "UPDATE CANDIDATE_DETAILS SET ATTEMPTED_STATUS = '0' WHERE ID = '" + candidateDetailsId + "';";
				logger.info("UPDATE QUERY - " + updateCandidateDetails);
				int deleteStatus = DBConnect.updateData(con, updateCandidateDetails);

				if (deleteStatus == -1) {

					throw new Exception("Error updating CANDIDATE_DETAILS table for Register Number: "
							+ registerNumber);
				}

				logger.info("Deletion successful in CANDIDATE_RESPONSE for Register Number: " + registerNumber);

				message = "Reset Successful for MCQ Questions";
			}

			//Deleting Coding Questions
			else if(questionType.equals("Coding")) {

				logger.info("Inside Coding case");
				logger.info("SQL QUery - " + deleteAllResponseCoding);
//				int deleteStatusCoding = DBConnect.updateData(con, deleteAllResponseCoding);
//
//				if (deleteStatusCoding == -1) {
//
//					throw new Exception("Error deleting records in CANDIDATE_CODING_RESPONSE for Register Number: "
//							+ registerNumber);
//				}

				String updateCandidateDetails = "UPDATE CANDIDATE_DETAILS SET ATTEMPTED_STATUS = '1' WHERE ID = '" + candidateDetailsId + "';";
				logger.info("SQL QUery - " + updateCandidateDetails);
				int deleteStatus = DBConnect.updateData(con, updateCandidateDetails);

				if (deleteStatus == -1) {

					throw new Exception("Error updating CANDIDATE_DETAILS table for Register Number: "
							+ registerNumber);
				}

				logger.info("Deletion successful in CANDIDATE_CODING_RESPONSE for Register Number: " + registerNumber);

				message = "Reset Successful for Coding Questions";
			}

			else {
				throw new Exception("Question type not handled");
			}

		} catch (Exception e) {
			statusMessage = false;
			message = "Error while restting details - " + e;
			con.close();
		}

		responseParameterList.put("RegisterNumber", registerNumber);
		responseParameterList.put("Status", message);
		responseParameterList.put("QuestionType", questionType);

		returnResponseObject.put("ParameterList", responseParameterList);
		returnResponseObject.put("Success", statusMessage);

		return returnResponseObject;

	}

	@SuppressWarnings("unchecked")
	public JSONObject testTakenStatus() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		boolean statusMessage = true;
		String reason = "";
		registerNumber = (String) this.candidateDetailsJSON.get("RegisterNumber");
		questionType = (String) this.candidateDetailsJSON.get("QuestionType");

		String sqlQueryStatusCheck = "SELECT ATTEMPTED_STATUS FROM CANDIDATE_DETAILS WHERE REG_NO = '" + registerNumber + "';";

		try {
			ResultSet rs = DBConnect.selectData(con, sqlQueryStatusCheck);
			while(rs.next()) {
				int attempted_status = rs.getInt(1);
				if(questionType.equalsIgnoreCase("MCQ")) {
					if(attempted_status == 0) {
						reason = "MCQ test not taken";
					}
					else {
						throw new Exception("MCQ");
					}
				}
				if(questionType.equalsIgnoreCase("Coding")) {
					if(attempted_status == 0 || attempted_status == 1) {
						reason = "Coding test not taken";
					}
					else {
						throw new Exception("Coding");
					}
				}
			}
		} catch (Exception e) {
			statusMessage = false;
			reason = "Test already taken - " + e;
			con.close();
		}

		responseParameterList.put("RegisterNumber", registerNumber);
		responseParameterList.put("Status", reason);
		responseParameterList.put("QuestionType", questionType);

		returnResponseObject.put("ParameterList", responseParameterList);
		returnResponseObject.put("Success", statusMessage);

		return returnResponseObject;

	}
}