package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.SystemConfig;
import evertz.evertz_interview_backend.application_server.logger;

public class CodingQuestionHandler {

	// From the calling class
	String candidateRegisterNumber = null;

	// From DB
	int candidateDetailsId;
	int candidateSelectedModule;
	int numberOfCodingQuestionsInTheModule;
	int[] questionIdsArray;
	String candidateSelectedModuleName = null;
	String[] codingQuestionsArray = null;
	String[] shuffledQuestionsIdArray = null;
	String[] shuffledQuestionsArray = null;

	// From SystemConfig.json file
	int numberOfQuestionsToGet;

	public CodingQuestionHandler(String candidateRegisterNumber) throws Exception {
		this.candidateRegisterNumber = candidateRegisterNumber;

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(candidateRegisterNumber);
	}

	@SuppressWarnings("unchecked")
	public JSONObject generateQuestions() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			generateRequiredData();

			ArrayList<Integer> list = new ArrayList<Integer>();

			for (int i = 0; i < this.numberOfCodingQuestionsInTheModule; i++) {
				list.add(this.questionIdsArray[i]);
			}

			Collections.shuffle(list);

			for (int i = 0; i < this.numberOfQuestionsToGet; i++) {
				this.shuffledQuestionsIdArray[i] = String.valueOf(list.get(i + 1));
			}

			String deleteQueryForCandidateCoding = "DELETE FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = '"
					+ this.candidateDetailsId + "';";

			logger.info(deleteQueryForCandidateCoding);

			int deleteQueryForCandidateCodingResult = DBConnect.updateData(con, deleteQueryForCandidateCoding);

			if (deleteQueryForCandidateCodingResult < 0) {

				throw new Exception("Unable to delete already existing questions for Register Number: "
						+ this.candidateRegisterNumber);
			}

			for (int j = 0; j < this.numberOfQuestionsToGet; j++) {

				String insertQueryForCandidateCoding = "INSERT INTO CANDIDATE_CODING_RESPONSE (CANDIDATE_DETAILS_ID, CODING_QUESTION_ID, COMPILATION_STATUS, RUN_STATUS, ATTEMPTED_STATUS, CODING_MARKS) VALUES ("
						+ this.candidateDetailsId + " , " + this.shuffledQuestionsIdArray[j]
						+ " , b'0', b'0', b'0', 0);";

				int insertQueryForCandidateCodingResult = DBConnect.updateData(con, insertQueryForCandidateCoding);

				if (insertQueryForCandidateCodingResult < 0) {

					throw new Exception(
							"Unable to add coding questions for Register Number: " + this.candidateRegisterNumber);
				}
			}

			logger.info("Coding Questions Insertion Successful for Register Number: " + this.candidateRegisterNumber);

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();

			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("Reason", e.getMessage());
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;
		}
	}

	@SuppressWarnings({ "unchecked" })
	public JSONObject getQuestions() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			generateRequiredData();

			ResultSet rs = null;
			ResultSet programResultSet = null;

			JSONArray questionDetailJSONArray = new JSONArray();

			String selectQueryForProgramIds = "SELECT DISTINCT CODING_QUESTION_ID FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = "
					+ this.candidateDetailsId + ";";

			logger.info(selectQueryForProgramIds);

			rs = DBConnect.selectData(con, selectQueryForProgramIds);

			while (rs.next()) {

				JSONObject questionDetails = new JSONObject();

				String selectQueryForPrograms = "SELECT QUESTION, BASE_CODE FROM CODING_QUESTIONS WHERE ID = " + rs.getString(1)
						+ " ;";

				logger.info(selectQueryForPrograms);

				programResultSet = DBConnect.selectData(con, selectQueryForPrograms);

				while (programResultSet.next()) {

					questionDetails.put("QId", rs.getString(1));
					questionDetails.put("Question", programResultSet.getString(1));
					questionDetails.put("ModuleName", this.candidateSelectedModuleName);
					questionDetails.put("BaseCode", programResultSet.getString(2));

				}

				questionDetailJSONArray.add(questionDetails);
			}

			if (questionDetailJSONArray.isEmpty()) {

				throw new Exception("No Questions generated for Register number: " + this.candidateRegisterNumber);
			}

			rs = null;
			programResultSet = null;

			JSONObject eachQuestionJSON = new JSONObject();
			eachQuestionJSON.put("Questions", questionDetailJSONArray);

			responseParameterList.put("QuestionsList", eachQuestionJSON);

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
			responseParameterList.put("QuestionType", "Coding");

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.info("Coding Questions Get Successful for Register Number: " + this.candidateRegisterNumber);

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("Reason", e.getMessage());
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}
	}

	private void generateRequiredData() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			this.numberOfQuestionsToGet = Integer.parseInt(SystemConfig.getSystemConfig("NumberOfCodingQuestions"));
			this.shuffledQuestionsIdArray = new String[this.numberOfQuestionsToGet];

			ResultSet rs = null;

			String candidateDetailsIdQuery = "SELECT ID, MODULE_ID FROM CANDIDATE_DETAILS WHERE REG_NO = '"
					+ this.candidateRegisterNumber + "';";

			logger.info(candidateDetailsIdQuery);

			rs = DBConnect.selectData(con, candidateDetailsIdQuery);

			while (rs.next()) {
				this.candidateDetailsId = Integer.parseInt(rs.getString(1));
				this.candidateSelectedModule = Integer.parseInt(rs.getString(2));
			}
			rs = null;

			if (this.candidateDetailsId == 0) {

				throw new Exception(
						"Unable to fetch Candidate Details ID for Register Number: " + this.candidateRegisterNumber);
			}

			String candidateSelectedModuleNameQuery = "SELECT NAME FROM MODULES WHERE ID = '"
					+ this.candidateSelectedModule + "';";

			logger.info(candidateSelectedModuleNameQuery);

			rs = DBConnect.selectData(con, candidateSelectedModuleNameQuery);

			while (rs.next()) {
				this.candidateSelectedModuleName = (String) (rs.getString(1));
			}
			rs = null;

			if (this.candidateSelectedModule == 0) {

				throw new Exception("Unable to fetch Candidate Selected Module for Register Number: "
						+ this.candidateRegisterNumber);
			}

			String numberOfCodingQuestionsInTheModuleQuery = "SELECT count(ID) as IDCount FROM CODING_QUESTIONS WHERE MODULE_ID = '"
					+ this.candidateSelectedModule + "';";

			logger.info(candidateSelectedModuleNameQuery);

			rs = DBConnect.selectData(con, numberOfCodingQuestionsInTheModuleQuery);

			while (rs.next()) {
				this.numberOfCodingQuestionsInTheModule = Integer.parseInt(rs.getString(1));
			}
			rs = null;

			if (this.numberOfCodingQuestionsInTheModule == 0) {

				throw new Exception("Unable to fetch Candidate Selected Module for Register Number: "
						+ this.candidateRegisterNumber);
			}

			String codingQuestionsInTheModuleQuery = "SELECT * FROM CODING_QUESTIONS WHERE MODULE_ID = '"
					+ this.candidateSelectedModule + "';";

			logger.info(codingQuestionsInTheModuleQuery);

			rs = DBConnect.selectData(con, codingQuestionsInTheModuleQuery);

			this.questionIdsArray = new int[this.numberOfCodingQuestionsInTheModule];
			this.codingQuestionsArray = new String[this.numberOfCodingQuestionsInTheModule];

			int i = 0;

			while (rs.next()) {
				this.questionIdsArray[i] = Integer.parseInt(rs.getString(1));
				this.codingQuestionsArray[i] = (String) rs.getString(2);
				i++;
			}

			if (this.questionIdsArray[0] == 0 || codingQuestionsArray[0].isEmpty()) {

				throw new Exception(
						"Unable to fetch Coding question for Register Number: " + this.candidateRegisterNumber);
			}

			rs = null;

			con.close();

		} catch (Exception e) {

			con.close();
			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
}
