package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class GenerateResults {
	//@SuppressWarnings("unused")
		private JSONObject JSONRequestMessage;
		static int candidateRegistrationId;	
		String response = "";

		
		
		public GenerateResults(JSONObject JSONRequestMessage) {
			
			this.JSONRequestMessage = JSONRequestMessage;
			System.out.println("In Constructor Received JSON message : " + JSONRequestMessage);
		}
		
		
		@SuppressWarnings("unchecked")
		public JSONObject getJsonList() throws Exception {
			
			ConnectToDatabase DBConnect = new ConnectToDatabase();
			Connection con = DBConnect.getConnection();
			String message = null;
			boolean statusMessage = true;
			JSONArray allCandidate = null;
			String collegeId = (String) this.JSONRequestMessage.get("CollegeId");
			try {
				String listAllCandidateIDs = "SELECT ID FROM CANDIDATE_DETAILS WHERE COLLEGE_DETAILS_ID = '" + collegeId + "';";
				
				ResultSet rs = DBConnect.selectData(con, listAllCandidateIDs);
				
				List<Integer> candidateIDList = new ArrayList<>();
				
				while(rs.next()) {
					candidateIDList.add(rs.getInt(1));
				}
				allCandidate = new JSONArray();
				for(Integer id : candidateIDList) {
					
					JSONObject eachCandidateData = new JSONObject();
					JSONObject eachCandidateScore = new JSONObject();
					int totalScoreEachStudent = 0;
					String getNameQuery = "SELECT NAME,REG_NO FROM CANDIDATE_DETAILS WHERE ID = '" + id + "';";
					String nameOfEachCandidate = "";
					String registerNumber = "";
					ResultSet rsn = DBConnect.selectData(con, getNameQuery);
					while(rsn.next()) {
						nameOfEachCandidate = rsn.getString(1);
						registerNumber = rsn.getString(2);
					}
					
					//Getting Coding marks
					String queryForCodingMarks = "SELECT CODING_MARKS FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = '" + id + "';";
					
					ResultSet rsc = DBConnect.selectData(con, queryForCodingMarks);
					int codingMarks = 0;
					while(rsc.next()) {
						codingMarks = rsc.getInt(1);
					}
					totalScoreEachStudent += (int) codingMarks;
					eachCandidateScore.put("Coding", codingMarks);
					
					String modulesForEachCandidate = "select M.ID FROM MODULES M WHERE M.MANDATORY_MODULE = 1 UNION SELECT M.ID "
							+ "FROM MODULES M JOIN CANDIDATE_DETAILS CD ON CD.MODULE_ID = M.ID WHERE CD.ID = '" + id + "';";
					
					ResultSet rsm = DBConnect.selectData(con, modulesForEachCandidate);
					
					List<Integer> moduleIDList = new ArrayList<>();
					
					while(rsm.next()) {
						System.out.println(rsm.getInt(1));
						moduleIDList.add(rsm.getInt(1));
					}
					
					for(Integer mid : moduleIDList) {
						String moduleWiseScore = "SELECT COUNT(CR.QUESTION_ID) FROM CANDIDATE_RESPONSE AS CR " + 
								"JOIN QUESTIONS AS Q ON Q.ID = CR.QUESTION_ID "
								+ "JOIN CANDIDATE_DETAILS AS CD ON CD.ID = CR.CANDIDATE_DETAILS_ID " 
								+ "JOIN MODULES AS M ON M.ID = Q.MODULE_ID WHERE M.ID = '" + mid + "'AND "
								+ "CR.CANDIDATE_ANSWER=Q.ANSWER AND CR.CANDIDATE_DETAILS_ID = " + id + ";"; 
						
						//logger.info(moduleWiseScore);
						String moduleNameQuery = "SELECT NAME FROM MODULES WHERE ID = '" + mid + "';";
						ResultSet moduleName = DBConnect.selectData(con, moduleNameQuery);
						String moduleNameStr = "";
						while(moduleName.next()) {
							moduleNameStr = moduleName.getString(1);
						}
						
						int moduleScore = 0;
						
						System.out.println(moduleWiseScore);
						ResultSet score = DBConnect.selectData(con, moduleWiseScore);
						if(score.next() == false) {
							moduleScore = 0;
						}
						else {
							moduleScore = score.getInt(1);
						}
						//logger.info("Each module Score - " + moduleNameStr + " Score - " + moduleScore);
						if(mid == moduleIDList.get(moduleIDList.size()-1)) {
							eachCandidateScore.put("OptionalModule", moduleNameStr);
							eachCandidateScore.put("Score", moduleScore);
							totalScoreEachStudent += (int) moduleScore;
							continue;
						}
						totalScoreEachStudent += (int) moduleScore;
						eachCandidateScore.put(moduleNameStr, moduleScore);
					}
					logger.info("Register Number - " + registerNumber + " Total Score - " + totalScoreEachStudent);
					eachCandidateScore.put("Total", totalScoreEachStudent);
					eachCandidateData.put("RegisterNumber", registerNumber);
					eachCandidateData.put("Name", nameOfEachCandidate);
					eachCandidateData.put("Score", eachCandidateScore);
					allCandidate.add(eachCandidateData);
					message = "Result generated";
					statusMessage = true;
				}
			} catch (Exception e) {
				statusMessage = false;
				message = "Error while generating result - " + e;
				con.close();
				logger.error(e);
			}
		JSONObject responseJson = new JSONObject();
		responseJson.put("ParameterList", allCandidate);
		responseJson.put("Success", statusMessage);
		responseJson.put("Result", message);
		
		return responseJson;
		} 
}
