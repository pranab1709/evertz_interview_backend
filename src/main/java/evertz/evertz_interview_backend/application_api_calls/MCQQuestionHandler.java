package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class MCQQuestionHandler {

	String candidateRegisterNumber = null;
	String responseJSONString = null;

	public MCQQuestionHandler(String candidateRegisterNumber) throws Exception {

		this.candidateRegisterNumber = candidateRegisterNumber;

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(candidateRegisterNumber);

	}

	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject generateQuestions() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		ResultSet rs = null;

		// List for all questions
		String success = "true";
		String status = "";
		String statusMessage = "";
		String candidateDetailsId = null;
		int countOfQuestionsEachModule = 10;
		List<Integer> questionIDList = new ArrayList<>();

		String attemptedStatus = "b'0'";
		String allQuestionInsertQuery = "";
		int updateStatus = 0;

		try {

			// Getting all modules
			String queryToFetchModuleIDs = "SELECT ID FROM MODULES where MANDATORY_MODULE = 1 "
					+ "UNION SELECT MODULE_ID FROM CANDIDATE_DETAILS where REG_NO = '" + this.candidateRegisterNumber
					+ "';";

			logger.info(queryToFetchModuleIDs);

			rs = DBConnect.selectData(con, queryToFetchModuleIDs);

			List<Integer> moduleIDList = new ArrayList<>();

			while (rs.next()) {

				moduleIDList.add(rs.getInt(1));
			}

			if (moduleIDList == null) {

				throw new Exception("No modules available for Register number: " + this.candidateRegisterNumber);
			}

			rs = null;

			// Getting all question IDs and shuffling them to generate random questions and
			// storing in questions ID list.
			for (int moduleID : moduleIDList) {

				String queryToFetchQuestionIDs = "SELECT ID FROM QUESTIONS WHERE MODULE_ID = '" + moduleID + "';";

				logger.info(queryToFetchQuestionIDs);

				ResultSet questionresult = DBConnect.selectData(con, queryToFetchQuestionIDs);

				List<Integer> tempQuestionList = new ArrayList<>();

				while (questionresult.next()) {

					tempQuestionList.add(questionresult.getInt(1));

				}

				if (tempQuestionList == null) {

					throw new Exception("No questions available for Register number: " + this.candidateRegisterNumber
							+ " & Module: " + moduleID);
				}

				// Shuffling the question
				Collections.shuffle(tempQuestionList);

				for (int itr = 0; itr < countOfQuestionsEachModule; itr++) {

					questionIDList.add(tempQuestionList.get(itr));
				}
			}

			// Getting candidate ID from Registration number
			String candidateDetailsIdQuery = "SELECT ID FROM CANDIDATE_DETAILS WHERE REG_NO = '"
					+ this.candidateRegisterNumber + "';";

			logger.info(candidateDetailsIdQuery);

			ResultSet rg = DBConnect.selectData(con, candidateDetailsIdQuery);

			while (rg.next()) {

				candidateDetailsId = rg.getString(1);
			}

			if (candidateDetailsId == null) {

				throw new Exception(
						"Unable to fetch Candidate Details ID for Register Number: " + this.candidateRegisterNumber);
			}

			// Checking if questions are already generated for candidate. If generated
			// delete all entry from Candidate response and add new questions.
			String queryToFindAlreadyExistingData = "SELECT CANDIDATE_DETAILS_ID FROM CANDIDATE_RESPONSE WHERE CANDIDATE_DETAILS_ID = ' "
					+ candidateDetailsId + " ';";

			logger.info(queryToFindAlreadyExistingData);

			ResultSet cqgs = DBConnect.selectData(con, queryToFindAlreadyExistingData);

			if (cqgs.next() == false) {

				logger.info("No data found in CANDIDATE_DETAILS for Register Number: " + this.candidateRegisterNumber
						+ ". Good to proceed.");

			} else {

				logger.info("Data found in CANDIDATE_DETAILS for Register Number: " + this.candidateRegisterNumber
						+ ". Deleting and proceeding.");

				String deleteCandidateResponseQuery = "DELETE from CANDIDATE_RESPONSE WHERE CANDIDATE_DETAILS_ID = ' "
						+ candidateDetailsId + " ';";

				int deleteStatus = DBConnect.updateData(con, deleteCandidateResponseQuery);

				if (deleteStatus == -1) {

					throw new Exception("Error deleting records in CANDIDATE_RESPONSE for Register Number: "
							+ this.candidateRegisterNumber);
				}

				logger.info("Deletion successful in CANDIDATE_RESPONSE for Register Number: "
						+ this.candidateRegisterNumber);

			}

			for (int questionID : questionIDList) {

				allQuestionInsertQuery = "INSERT INTO CANDIDATE_RESPONSE (CANDIDATE_DETAILS_ID, QUESTION_ID, ATTEMPTED_STATUS) "
						+ "VALUES('" + candidateDetailsId + "', '" + questionID + "', " + attemptedStatus + ");";

				updateStatus = DBConnect.updateData(con, allQuestionInsertQuery);

				if (updateStatus == -1) {

					throw new Exception("Data insertion failed for Register Number: " + this.candidateRegisterNumber
							+ " for QuestionId: " + questionID);

				} else {

					logger.info("Data insertion successful for Register Number: " + this.candidateRegisterNumber
							+ " for QuestionId: " + questionID);
				}
			}

			logger.info("All Questions Insertion Successful for Register Number: " + this.candidateRegisterNumber);

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("Reason", e.getMessage());
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject getQuestions() throws Exception {

		JSONObject returnResponseObject = new JSONObject();

		JSONObject parameterList = new JSONObject();

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		String candidateDetailsId = null;

		try {

			// Getting Candidate ID to be used while fetching question.
			String getCandidateDetailsIdQuery = "SELECT ID FROM CANDIDATE_DETAILS where REG_NO = '"
					+ this.candidateRegisterNumber + "';";

			logger.info(getCandidateDetailsIdQuery);

			ResultSet rg = DBConnect.selectData(con, getCandidateDetailsIdQuery);

			while (rg.next()) {

				candidateDetailsId = rg.getString(1);

			}

			if (candidateDetailsId == null) {

				throw new Exception(
						"Unable to fetch Candidate Details ID for Register Number: " + this.candidateRegisterNumber);
			}

			// Fetching all questions for candidate
			String fetchAllQuestionSQLQuery = "SELECT q.ID, q.QUESTION, q.OPTION1, q.OPTION2, q.OPTION3, q.OPTION4, m.NAME FROM QUESTIONS q"
					+ " JOIN MODULES m on m.ID = q.MODULE_ID and q.ID IN (SELECT QUESTION_ID FROM CANDIDATE_RESPONSE where CANDIDATE_DETAILS_ID = '"
					+ candidateDetailsId + "');";

			logger.info(fetchAllQuestionSQLQuery);

			ResultSet allQuestions = DBConnect.selectData(con, fetchAllQuestionSQLQuery);

			int idFromBackend = 1;

			JSONArray eachQuestionJSONArray = new JSONArray();

			// Adding all questions in JSON Format

			while (allQuestions.next()) {
				
				//Adding candidate answer
				String getAnswerForEachQuestion = "SELECT CANDIDATE_ANSWER FROM CANDIDATE_RESPONSE WHERE CANDIDATE_DETAILS_ID = " + candidateDetailsId + " AND "
						+ "QUESTION_ID = " + allQuestions.getInt(1) + ";";
				int answerOfEachQuestion = 0;
				
				ResultSet answerresult = DBConnect.selectData(con, getAnswerForEachQuestion);
				
				while(answerresult.next()) {
					answerOfEachQuestion = answerresult.getInt(1);
				}

				JSONArray optionList = new JSONArray();

				int itr = 1;

				for (int i = 1; i <= 4; i++) {

					int j = i + 2;
					if (allQuestions.getString(j).equalsIgnoreCase(null)
							|| allQuestions.getString(j).equalsIgnoreCase("null")) {

					} else {

						JSONObject questionOption = new JSONObject();
						questionOption.put("OptionId", itr);
						questionOption.put("OptionValue", allQuestions.getString(j));
						optionList.add(questionOption);
						itr++;

					}

				}

				JSONObject questionDetails = new JSONObject();

				questionDetails.put("Id", idFromBackend);
				questionDetails.put("QId", allQuestions.getInt(1));
				questionDetails.put("Question", allQuestions.getString(2));
				questionDetails.put("OptionSelected", answerOfEachQuestion);
				questionDetails.put("QuestionOptions", optionList);
				
				JSONArray questionDetailJSONArray = new JSONArray();
				questionDetailJSONArray.add(questionDetails);

				JSONObject eachQuestionJSON = new JSONObject();

				eachQuestionJSON.put("ModuleName", allQuestions.getString(7));

				eachQuestionJSON.put("Questions", questionDetailJSONArray);

				eachQuestionJSONArray.add(eachQuestionJSON);

				idFromBackend++;

				logger.info(questionDetails.toString());
			}

			parameterList.put("RegisterNumber", this.candidateRegisterNumber);
			parameterList.put("QuestionsList", eachQuestionJSONArray);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", parameterList);

			return returnResponseObject;

		} catch (Exception e) {

			parameterList.put("RegisterNumber", this.candidateRegisterNumber);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("Reason", e.getMessage());

			returnResponseObject.put("ParameterList", parameterList);

			logger.error(e);
			return returnResponseObject;

		}

	}
}
