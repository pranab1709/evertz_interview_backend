package evertz.evertz_interview_backend.application_api_calls;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.stream.Collectors;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.SystemConfig;
import evertz.evertz_interview_backend.application_server.logger;

public class SaveOrSubmitCodingAnswer {

	public String candidateRegisterNumber = null;
	public String codingQuestionId = null;
	public String candidateEnteredAnswer = null;
	public String compilationStatus = null;
	public String runStatus = null;
	public int attemptedStatus = 1;
	public String codingMarks = null;
	public int candidateDetailsId;

	// These are calculated from the previous variables
	public String candidateCodeFileName = null;
	public String candidateProgramLocation = null;
	public String candidateFileLocation = null;
	public String fileExtension = null;
	public String moduleName = null;
	public int moduleId;
	public String candidateCodeFileNameWithoutExtension = null;

	public SaveOrSubmitCodingAnswer(String candidateRegisterNumber, String questionId, String candidateEnteredAnswer)
			throws Exception {

		this.candidateRegisterNumber = candidateRegisterNumber;
		this.codingQuestionId = questionId;
		this.candidateEnteredAnswer = candidateEnteredAnswer;

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(candidateRegisterNumber);

		ResultSet rs;

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		String getCodingQuestionDetailsQuery = "SELECT * FROM CODING_QUESTIONS WHERE ID = '" + this.codingQuestionId
				+ "';";
		rs = DBConnect.selectData(con, getCodingQuestionDetailsQuery);
		while (rs.next()) {
			this.moduleId = rs.getInt(3);
		}
		rs = null;

		String moduleDetailsQuery = "SELECT * FROM MODULES WHERE ID = '" + this.moduleId + "';";
		rs = DBConnect.selectData(con, moduleDetailsQuery);
		while (rs.next()) {
			this.moduleName = rs.getString(2);
			this.fileExtension = rs.getString(4);
		}
		rs = null;

		this.candidateCodeFileNameWithoutExtension = this.fileExtension.toUpperCase() + "_"
				+ this.candidateRegisterNumber;
		this.candidateCodeFileName = this.candidateCodeFileNameWithoutExtension + "_Raw." + this.fileExtension;

		String operatingSystem = SystemConfig.getSystemConfig("OperatingSystem");

		this.candidateProgramLocation = SystemConfig.getSystemConfig("CandidateCodeLocation" + operatingSystem);

		this.candidateFileLocation = this.candidateProgramLocation + this.candidateCodeFileName;

	}

	public int saveAnswerToDatabase() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		int candidateDetailsIdResult = this.getCandidateDetailsId();

		if (candidateDetailsIdResult != 0) {

			throw new Exception(
					"Unable to fetch Candidate Details Id for Register number: " + this.candidateRegisterNumber);
		}

		String checkQuestionIdExists = "SELECT * FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = "
				+ this.candidateDetailsId + " AND CODING_QUESTION_ID = " + this.codingQuestionId + ";";

		logger.info(checkQuestionIdExists);

		ResultSet rs1 = DBConnect.selectData(con, checkQuestionIdExists);

		if (rs1.next()) {

			String saveQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET CANDIDATE_PROGRAM = '"
					+ this.candidateEnteredAnswer + "' WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

			logger.info(saveQuery);

			int updateStatus = DBConnect.updateData(con, saveQuery);

			con.close();

			if (updateStatus == -1) {

				return 1;

			} else {

				return 0;
			}
		}

		return 1;

	}

	public int saveAnswerToFile() throws Exception {

		String candidateAnswer = this.candidateEnteredAnswer.toString();

		logger.info(this.candidateFileLocation);

		Writer writer = null;

		try {

			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.candidateFileLocation)));

			writer.write(candidateAnswer);

			return 0;

		} catch (Exception e) {

			logger.error(e);
			return 1;

		} finally {

			try {

				writer.close();
			} catch (Exception ex) {

				logger.error(ex);
				return 1;

			}
		}
	}

	public int submitAnswer() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		int candidateDetailsIdResult = this.getCandidateDetailsId();

		if (candidateDetailsIdResult != 0) {

			throw new Exception(
					"Unable to fetch Candidate Details Id for Register number: " + this.candidateRegisterNumber);
		}

		String checkQuestionIdExists = "SELECT * FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = "
				+ this.candidateDetailsId + " AND CODING_QUESTION_ID = " + this.codingQuestionId + ";";

		logger.info(checkQuestionIdExists);

		ResultSet rs1 = DBConnect.selectData(con, checkQuestionIdExists);

		if (rs1.next()) {

			String submitQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET" + " ATTEMPTED_STATUS = b'1'"
					+ " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + " AND CODING_QUESTION_ID = "
					+ this.codingQuestionId + ";";

			logger.info(submitQuery);

			int updateStatus = DBConnect.updateData(con, submitQuery);

			String updateCandidateTable = "UPDATE CANDIDATE_DETAILS SET ATTEMPTED_STATUS = '2' WHERE REG_NO = '"
					+ this.candidateRegisterNumber + "';";

			int updateStatusCandidate = DBConnect.updateData(con, updateCandidateTable);

			con.close();

			if (updateStatus == -1 || updateStatusCandidate == -1) {

				return 1;

			} else {

				return 0;
			}
		} else {

			return 1;
		}
	}

	private int getCandidateDetailsId() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		String getCandidateDetailsIdQuery = "SELECT ID FROM CANDIDATE_DETAILS WHERE REG_NO = '"
				+ this.candidateRegisterNumber + "';";
		ResultSet rs = DBConnect.selectData(con, getCandidateDetailsIdQuery);

		while (rs.next()) {
			this.candidateDetailsId = rs.getInt(1);
		}

		con.close();

		if (this.candidateDetailsId == 0) {

			return 1;
		}

		return 0;
	}

	public String getSavedProgram() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		this.getCandidateDetailsId();

		String programData = null;

		String queryToFetchProgram = "SELECT CANDIDATE_PROGRAM FROM CANDIDATE_CODING_RESPONSE WHERE CANDIDATE_DETAILS_ID = '"
				+ this.candidateDetailsId + "' AND " + "CODING_QUESTION_ID = '" + this.codingQuestionId + "';";

		logger.info(queryToFetchProgram);

		ResultSet programContents = DBConnect.selectData(con, queryToFetchProgram);

		while (programContents.next()) {
			programData = programContents.getString(1);
		}

		con.close();

		return programData;

	}
	
	public String getSavedProgramFromFile() throws Exception {

		try {
			File file = new File(this.candidateFileLocation);

			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(file));

			return (org.apache.commons.io.IOUtils.toString(br));
		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}
	}
}
