/*
	Author: 		Shreelaxmi
	Created Date: 	10th September 2019
	Modified Date: 	19th September 2019
	Description: 	To save the mcq answers  On Save or On Submit and update database accordingly. 
	*/

package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;

/*
Author: 		Shreelakshmi
Created Date: 	23rd September 2019
Modified Date: 	23rd September 2019
Description: 	This class would just get the Request from the CandidateResponseDispatcher class and save the responses of the candidate for each question. 

 */

import java.sql.ResultSet;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class SaveOrSubmitMCQAnswer {

	public String candidateRegisterNumber = null;
	public String mcqQuestionId = null;
	public String candidateSelectedAnswer = null;
	public int attemptedStatus = 1;
	public int candidateDetailsId;

	public SaveOrSubmitMCQAnswer(String candidateRegisterNumber, String questionId, String candidateEnteredAnswer)
			throws Exception {
		
		this.candidateRegisterNumber = candidateRegisterNumber;
		this.mcqQuestionId = questionId;
		this.candidateSelectedAnswer = candidateEnteredAnswer;

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(candidateRegisterNumber);


	}

	public int saveAnswer() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		int candidateDetailsIdResult = this.getCandidateDetailsId();

		if (candidateDetailsIdResult != 0) {

			throw new Exception(
					"Unable to fetch Candidate Details Id for Register number: " + this.candidateRegisterNumber);
		}

		String checkQuestionIdExists = "SELECT * FROM CANDIDATE_RESPONSE WHERE CANDIDATE_DETAILS_ID = "
				+ this.candidateDetailsId + " AND QUESTION_ID = " + this.mcqQuestionId + ";";

		logger.info(checkQuestionIdExists);

		ResultSet rs1 = DBConnect.selectData(con, checkQuestionIdExists);		

		if (rs1.next()) {

			String saveQuery = "UPDATE CANDIDATE_RESPONSE SET CANDIDATE_ANSWER = '" + this.candidateSelectedAnswer
					+ "', ATTEMPTED_STATUS = b'1' " + " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId
					+ " AND " + "QUESTION_ID = " + this.mcqQuestionId + ";";

			int updateStatus = DBConnect.updateData(con, saveQuery);

			logger.info(saveQuery);

			con.close();
			if (updateStatus == -1) {

				return 1;

			} else {

				return 0;

			}
		}

		return 1;
	}

	public int submitAnswer() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		int candidateDetailsIdResult = this.getCandidateDetailsId();

		if (candidateDetailsIdResult != 0) {

			throw new Exception(
					"Unable to fetch Candidate Details Id for Register number: " + this.candidateRegisterNumber);
		}

		String submitQuery = " UPDATE CANDIDATE_DETAILS SET ATTEMPTED_STATUS = '1' WHERE REG_NO = '"
				+ this.candidateRegisterNumber + "';";

		logger.info(submitQuery);

		int updateStatus = DBConnect.updateData(con, submitQuery);
		con.close();

		if (updateStatus == -1) {

			return 1;

		} else {

			return 0;
		}
	}

	private int getCandidateDetailsId() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		String getCandidateDetailsIdQuery = "SELECT ID FROM CANDIDATE_DETAILS WHERE REG_NO = '"
				+ this.candidateRegisterNumber + "';";
		ResultSet rs = DBConnect.selectData(con, getCandidateDetailsIdQuery);

		while (rs.next()) {
			this.candidateDetailsId = rs.getInt(1);
		}

		con.close();
		if (this.candidateDetailsId == 0) {

			return 1;
		}

		return 0;
	}
}
