package evertz.evertz_interview_backend.application_api_calls;

import java.sql.Connection;

/*
 	Author: 		Sauravk
	Created Date: 	19th September 2019
	Modified Date: 	19th September 2019
	Description: 	Any information the UI needs to show in the HTML page. Eg: College, Degree and Branch data from DB.
*/

import java.sql.ResultSet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class UI {

	private JSONObject JSONRequestMessage;

	public UI(JSONObject JSONRequestMessage) {

		this.JSONRequestMessage = JSONRequestMessage;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject getJsonList() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ResultSet rs = null;

		String dropdownFieldValue = "";
		String dropDownKeyNameList = "";

		try {

			dropdownFieldValue = (String) this.JSONRequestMessage.get("UIDropdownField");

			String selectQuery = null;

			// Check condition and fetch the data and keyName
			if (dropdownFieldValue.equals("College")) {

				dropDownKeyNameList = "CollegeList";
				selectQuery = "SELECT * FROM COLLEGE_DETAILS WHERE CURRENT_STATUS = 1 ;";

			} else if (dropdownFieldValue.equals("Degree")) {

				dropDownKeyNameList = "DegreeList";
				selectQuery = "SELECT * FROM DEGREE;";
			} else if (dropdownFieldValue.equals("Branch")) {

				dropDownKeyNameList = "BranchList";
				selectQuery = "SELECT * FROM BRANCH;";
			} else if (dropdownFieldValue.equals("Modules")) {

				dropDownKeyNameList = "ModuleList";
				selectQuery = "SELECT * FROM MODULES WHERE MANDATORY_MODULE = 0;";
			} else {

				throw new Exception("No mapping found for " + dropdownFieldValue);
			}

			// query to fetch the details from the database
			logger.info(selectQuery);

			rs = DBConnect.selectData(con, selectQuery);

			JSONArray dropdownValuesFromDB = new JSONArray();

			while (rs.next()) {

				JSONObject eachData = new JSONObject();

				eachData.put("Id", rs.getInt(1));
				eachData.put("Name", rs.getString(2));

				dropdownValuesFromDB.add(eachData);

			}

			if (dropdownValuesFromDB == null) {

				throw new Exception("No Data found for " + dropDownKeyNameList);
			}

			responseParameterList.put(dropDownKeyNameList, dropdownValuesFromDB);
			responseParameterList.put("UIDropdownField", dropdownFieldValue);

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.info(returnResponseObject.toString());

			con.close();
			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("Reason", e.getMessage());
			responseParameterList.put("UIDropdownField", dropdownFieldValue);

			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);

			con.close();
			logger.error(e);
			return returnResponseObject;

		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject addNewUIData() throws Exception {
		
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		
		String dropdownFieldValue = "";
		String dropdownFieldOptionStr = "";
		String dropdownFieldOption = "";
		boolean successMessage = true;
		String message = "";
		
		try {
			dropdownFieldOptionStr = (String) this.JSONRequestMessage.get("UIDropdownField");
			dropdownFieldValue = (String) this.JSONRequestMessage.get("Data");
			
			if(dropdownFieldOptionStr.isEmpty() || dropdownFieldOptionStr.isBlank() || dropdownFieldValue.isEmpty() || dropdownFieldValue.isBlank()) {
				throw new Exception("Dropdown Options and values are not proper, please provide proper value");
			}
			
			if(dropdownFieldOptionStr.equalsIgnoreCase("College")) {
				dropdownFieldOption = "COLLEGE_DETAILS";
			}
			else if(dropdownFieldOptionStr.equalsIgnoreCase("Branch")) {
				dropdownFieldOption = "BRANCH";
			}
			else if(dropdownFieldOptionStr.equalsIgnoreCase("Degree")) {
				dropdownFieldOption = "DEGREE";
			}
			else {
				throw new Exception("Dropdown option not found. Please select valid name.");
			}
			
			String checkQuery = "SELECT NAME FROM " + dropdownFieldOption + " WHERE NAME = '" + dropdownFieldValue + "';";
			logger.info(checkQuery);

			ResultSet rs = DBConnect.selectData(con, checkQuery);

			while (rs.next()) {
				String dropdownDataFromDB = rs.getString(1);

				if (dropdownDataFromDB.equals(dropdownFieldValue)) {

					throw new Exception(dropdownFieldOption + " already exists. Please add with different name.");
				}
			}
			
			String sqlQueryString = "INSERT INTO " + dropdownFieldOption + " (NAME) VALUES('" + dropdownFieldValue + "');";
			
			logger.info(sqlQueryString);

			int res = DBConnect.updateData(con, sqlQueryString);

			if (res == -1) {

				throw new Exception("INSERT Query Failed: " + sqlQueryString);
			} else {

				logger.info(dropdownFieldValue + " added successfully");
				successMessage = true;
				message = dropdownFieldValue + " added successfully";
			}
			
		} catch (Exception e) {
			successMessage = false;
			message = "Some error occured - " + e;
			logger.error(e);
		}
		
		responseParameterList.put("UIDropdownField", dropdownFieldOption);
		responseParameterList.put("Data", dropdownFieldValue);
		responseParameterList.put("Reason", message);
		returnResponseObject.put("Success", successMessage);
		returnResponseObject.put("ParameterList", responseParameterList);

		con.close();

		return returnResponseObject;
		
	}
	
}