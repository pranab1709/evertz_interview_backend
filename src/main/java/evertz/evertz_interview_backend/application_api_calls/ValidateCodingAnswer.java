package evertz.evertz_interview_backend.application_api_calls;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.stream.Collectors;

import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator.CProgramCodeValidator;
import evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator.CppProgramCodeValidator;
import evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator.JavaCodeValidator;
import evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator.PythonProgramCodeValidator;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.SystemConfig;
import evertz.evertz_interview_backend.application_server.logger;

public class ValidateCodingAnswer {

	// This is during object creation
	String codingQuestionId;
	String registerNumber;

	// These are auto populated from the DB
	// This is from the coding_questions table
	int moduleId;
	String baseClassName = null;

	// This is from the modules table
	String moduleName = null;
	String fileExtension = null;

	// This is from candidate_details table
	int candidateDetailsId;

	// This is from the candidate_coding_response table
	String candidateEnteredAnswer = null;

	// This is from the coding_test_cases table
	int numberOfTestCases;
	String[] testCaseArray = null;
	String[] testCaseAnswerArray = null;
	String[] testCaseHiddenStatus = null;
	String[] testCaseCompilationStatus = null;
	String[] testCaseRunStatus = null;
	int codingMarks;

	// These are calculated from the previous variables
	String candidateCodeFileName = null;
	String candidateProgramLocation = null;
	String candidateFileLocation = null;
	String candidateCodeRawFileName = null;
	String candidateRawFileLocation = null;

	String candidateCodeFileNameWithoutExtension = null;

	// This constructor populates all the variables
	public ValidateCodingAnswer(String registerNumber, String codingQuestionId) throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		this.codingQuestionId = codingQuestionId;
		this.registerNumber = registerNumber;

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(this.registerNumber);

		String getCodingQuestionDetailsQuery = "SELECT * FROM CODING_QUESTIONS WHERE ID = '" + this.codingQuestionId
				+ "';";
		ResultSet rs = DBConnect.selectData(con, getCodingQuestionDetailsQuery);
		while (rs.next()) {
			this.moduleId = rs.getInt(3);
			this.baseClassName = rs.getString(4);
		}
		rs = null;

		String moduleDetailsQuery = "SELECT * FROM MODULES WHERE ID = '" + this.moduleId + "';";
		rs = DBConnect.selectData(con, moduleDetailsQuery);
		while (rs.next()) {
			this.moduleName = rs.getString(2);
			this.fileExtension = rs.getString(4);
		}
		rs = null;

		String candidateDetailsIdQuery = "SELECT ID FROM CANDIDATE_DETAILS WHERE REG_NO = '" + this.registerNumber
				+ "';";
		rs = DBConnect.selectData(con, candidateDetailsIdQuery);
		while (rs.next()) {
			this.candidateDetailsId = rs.getInt(1);
		}
		rs = null;

		String codingTestCasesCount = "SELECT count(ID) as COUNT_OF_TEST_CASES FROM CODING_TEST_CASES WHERE CODING_QUESTION_ID = '"
				+ this.codingQuestionId + "';";
		rs = DBConnect.selectData(con, codingTestCasesCount);
		while (rs.next()) {
			this.numberOfTestCases = rs.getInt(1);
		}
		rs = null;

		this.testCaseArray = new String[this.numberOfTestCases];
		this.testCaseAnswerArray = new String[this.numberOfTestCases];
		this.testCaseHiddenStatus = new String[this.numberOfTestCases];
		this.testCaseCompilationStatus = new String[this.numberOfTestCases];
		this.testCaseRunStatus = new String[this.numberOfTestCases];

		String codingTestCases = "SELECT * FROM CODING_TEST_CASES WHERE CODING_QUESTION_ID = '" + this.codingQuestionId
				+ "';";
		rs = DBConnect.selectData(con, codingTestCases);
		int i = 0;
		while (rs.next()) {
			this.testCaseArray[i] = rs.getString(3);
			this.testCaseAnswerArray[i] = rs.getString(4);
			this.testCaseHiddenStatus[i] = rs.getString(5);
			i++;
		}
		rs = null;

		this.candidateCodeFileNameWithoutExtension = this.fileExtension.toUpperCase() + "_" + this.registerNumber;
		this.candidateCodeFileName = this.candidateCodeFileNameWithoutExtension + "." + this.fileExtension;
		this.candidateCodeRawFileName = this.candidateCodeFileNameWithoutExtension + "_Raw." + this.fileExtension;

		String operatingSystem = SystemConfig.getSystemConfig("OperatingSystem");

		this.candidateProgramLocation = SystemConfig.getSystemConfig("CandidateCodeLocation" + operatingSystem);

		this.candidateRawFileLocation = this.candidateProgramLocation + this.candidateCodeRawFileName;
		this.candidateFileLocation = this.candidateProgramLocation + this.candidateCodeFileName;

		this.candidateEnteredAnswer = readProgramFromFile(this.candidateRawFileLocation);

		con.close();
	}

	public int getNumberOfTestCases() {

		return this.numberOfTestCases;

	}

	public String[] getTestCaseArray() {

		return this.testCaseArray;
	}

	public String[] getTestCaseOutput() {

		return this.testCaseAnswerArray;
	}

	public String[] getTestCaseHiddenStatus() {

		return this.testCaseHiddenStatus;
	}

	public int compileProgram() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		if (this.moduleName.equals("Java")) {

			try {

				int exportedFileResult = exportToFile();

				if (exportedFileResult != 0) {

					throw new Exception(
							"Unable to export program to file for Candidate Details Id: " + this.candidateDetailsId);
				}

				JavaCodeValidator jcv = new JavaCodeValidator(this.candidateProgramLocation, this.candidateFileLocation,
						this.candidateCodeFileNameWithoutExtension);

				int compilationStatus = jcv.compileProgram();

				if (compilationStatus != 0) {

					throw new Exception("Unable to compile program for Candidate Details ID: " + this.candidateDetailsId
							+ " for QuestionId: " + this.codingQuestionId);

				}

				String updateCompilationStatus = "UPDATE CANDIDATE_CODING_RESPONSE SET COMPILATION_STATUS = b'1' WHERE CANDIDATE_DETAILS_ID = "
						+ this.candidateDetailsId + " AND CODING_QUESTION_ID = " + this.codingQuestionId + ";";

				logger.info(updateCompilationStatus);

				int compilationStatusFromQuery = DBConnect.updateData(con, updateCompilationStatus);

				con.close();
				return compilationStatusFromQuery;

			} catch (Exception e) {

				con.close();
				logger.error(e);
				throw new Exception(e.getMessage());
			}

		} else if (this.moduleName.equals("C")) {

			try {

				int exportedFileResult = exportToFile();

				if (exportedFileResult != 0) {

					throw new Exception(
							"Unable to export program to file for Candidate Details Id: " + this.candidateDetailsId);
				}

				CProgramCodeValidator cpcv = new CProgramCodeValidator(this.candidateProgramLocation,
						this.candidateFileLocation, this.candidateCodeFileNameWithoutExtension);

				int compilationStatus = cpcv.compileProgram();

				if (compilationStatus != 0) {

					throw new Exception("Unable to compile program for Candidate Details ID: " + this.candidateDetailsId
							+ " for QuestionId: " + this.codingQuestionId);

				}

				String updateCompilationStatus = "UPDATE CANDIDATE_CODING_RESPONSE SET COMPILATION_STATUS = b'1' WHERE CANDIDATE_DETAILS_ID = "
						+ this.candidateDetailsId + " AND CODING_QUESTION_ID = " + this.codingQuestionId + ";";

				logger.info(updateCompilationStatus);

				int compilationStatusFromQuery = DBConnect.updateData(con, updateCompilationStatus);

				con.close();
				return compilationStatusFromQuery;

			} catch (Exception e) {

				con.close();

				throw new Exception(e.getMessage());
			}

		} else if (this.moduleName.equals("C++")) {

			try {

				int exportedFileResult = exportToFile();

				if (exportedFileResult != 0) {

					throw new Exception(
							"Unable to export program to file for Candidate Details Id: " + this.candidateDetailsId);
				}

				CppProgramCodeValidator cpcv = new CppProgramCodeValidator(this.candidateProgramLocation,
						this.candidateFileLocation, this.candidateCodeFileNameWithoutExtension);

				int compilationStatus = cpcv.compileProgram();

				if (compilationStatus != 0) {

					throw new Exception("Unable to compile program for Candidate Details ID: " + this.candidateDetailsId
							+ " for QuestionId: " + this.codingQuestionId);

				}

				String updateCompilationStatus = "UPDATE CANDIDATE_CODING_RESPONSE SET COMPILATION_STATUS = b'1' WHERE CANDIDATE_DETAILS_ID = "
						+ this.candidateDetailsId + " AND CODING_QUESTION_ID = " + this.codingQuestionId + ";";

				logger.info(updateCompilationStatus);

				int compilationStatusFromQuery = DBConnect.updateData(con, updateCompilationStatus);

				con.close();
				return compilationStatusFromQuery;

			} catch (Exception e) {

				con.close();

				throw new Exception(e.getMessage());
			}

		} else if (this.moduleName.equals("Python")) {

			try {

				int exportedFileResult = exportToFile();

				if (exportedFileResult != 0) {

					throw new Exception(
							"Unable to export program to file for Candidate Details Id: " + this.candidateDetailsId);
				}

				con.close();
				return 1;

			} catch (Exception e) {

				con.close();

				throw new Exception(e.getMessage());
			}

		} else {

			throw new Exception("No compilation module built for " + this.moduleName);
		}
	}

	public String[] runProgram() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			if (this.moduleName.equals("Java")) {
				int codingMarks = 0;
				String runStatus = "0";

				JavaCodeValidator jcv = new JavaCodeValidator(this.candidateProgramLocation, this.candidateFileLocation,
						this.candidateCodeFileNameWithoutExtension);

				for (int i = 0; i < this.numberOfTestCases; i++) {

					this.testCaseRunStatus[i] = jcv.runProgram(this.testCaseArray[i], this.testCaseAnswerArray[i]);

					if (this.testCaseRunStatus[i].equals(this.testCaseAnswerArray[i])) {

						++codingMarks;

					}
				}

				String updateCodingMarksQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET CODING_MARKS = '" + codingMarks
						+ "'" + " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int updateStatus = DBConnect.updateData(con, updateCodingMarksQuery);

				logger.info(updateCodingMarksQuery);

				if (updateStatus < 0) {

					throw new Exception("Unable to set Coding Marks to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				if (codingMarks == this.numberOfTestCases) {

					runStatus = "1";
				}

				String setRunStatusQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET RUN_STATUS = b'" + runStatus
						+ "' WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int setRunStatusQueryResult = DBConnect.updateData(con, setRunStatusQuery);

				logger.info(setRunStatusQuery);

				if (setRunStatusQueryResult < 0) {

					throw new Exception("Unable to set Run status to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				con.close();
				return this.testCaseRunStatus;

			} else if (this.moduleName.equals("C")) {

				int codingMarks = 0;
				String runStatus = "0";

				CProgramCodeValidator cpcv = new CProgramCodeValidator(this.candidateProgramLocation,
						this.candidateFileLocation, this.candidateCodeFileNameWithoutExtension);

				for (int i = 0; i < this.numberOfTestCases; i++) {

					this.testCaseRunStatus[i] = cpcv.runProgram(this.testCaseArray[i], this.testCaseAnswerArray[i]);

					if (this.testCaseRunStatus[i].equals(this.testCaseAnswerArray[i])) {

						++codingMarks;

					}
				}

				String updateCodingMarksQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET CODING_MARKS = '" + codingMarks
						+ "'" + " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int updateStatus = DBConnect.updateData(con, updateCodingMarksQuery);

				logger.info(updateCodingMarksQuery);

				if (updateStatus < 0) {

					throw new Exception("Unable to set Coding Marks to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				if (codingMarks == this.numberOfTestCases) {

					runStatus = "1";
				}

				String setRunStatusQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET RUN_STATUS = b'" + runStatus
						+ "' WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int setRunStatusQueryResult = DBConnect.updateData(con, setRunStatusQuery);

				logger.info(setRunStatusQuery);

				if (setRunStatusQueryResult < 0) {

					throw new Exception("Unable to set Run status to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				con.close();
				return this.testCaseRunStatus;

			} else if (this.moduleName.equals("C++")) {

				int codingMarks = 0;
				String runStatus = "0";

				CppProgramCodeValidator cpcv = new CppProgramCodeValidator(this.candidateProgramLocation,
						this.candidateFileLocation, this.candidateCodeFileNameWithoutExtension);

				for (int i = 0; i < this.numberOfTestCases; i++) {

					this.testCaseRunStatus[i] = cpcv.runProgram(this.testCaseArray[i], this.testCaseAnswerArray[i]);

					if (this.testCaseRunStatus[i].equals(this.testCaseAnswerArray[i])) {

						++codingMarks;

					}
				}

				String updateCodingMarksQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET CODING_MARKS = '" + codingMarks
						+ "'" + " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int updateStatus = DBConnect.updateData(con, updateCodingMarksQuery);

				logger.info(updateCodingMarksQuery);

				if (updateStatus < 0) {

					throw new Exception("Unable to set Coding Marks to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				if (codingMarks == this.numberOfTestCases) {

					runStatus = "1";
				}

				String setRunStatusQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET RUN_STATUS = b'" + runStatus
						+ "' WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int setRunStatusQueryResult = DBConnect.updateData(con, setRunStatusQuery);

				logger.info(setRunStatusQuery);

				if (setRunStatusQueryResult < 0) {

					throw new Exception("Unable to set Run status to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				con.close();
				return this.testCaseRunStatus;
			} else if (this.moduleName.equals("Python")) {

				int codingMarks = 0;
				String runStatus = "0";

				PythonProgramCodeValidator pcv = new PythonProgramCodeValidator(this.candidateProgramLocation,
						this.candidateFileLocation, this.candidateCodeFileNameWithoutExtension);

				for (int i = 0; i < this.numberOfTestCases; i++) {

					this.testCaseRunStatus[i] = pcv.runProgram(this.testCaseArray[i], this.testCaseAnswerArray[i]);

					if (this.testCaseRunStatus[i].equals(this.testCaseAnswerArray[i])) {

						++codingMarks;

					}
				}

				String updateCodingMarksQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET CODING_MARKS = '" + codingMarks
						+ "'" + " WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int updateStatus = DBConnect.updateData(con, updateCodingMarksQuery);

				logger.info(updateCodingMarksQuery);

				if (updateStatus < 0) {

					throw new Exception("Unable to set Coding Marks to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				if (codingMarks == this.numberOfTestCases) {

					runStatus = "1";
				}

				String setRunStatusQuery = "UPDATE CANDIDATE_CODING_RESPONSE SET RUN_STATUS = b'" + runStatus
						+ "' WHERE CANDIDATE_DETAILS_ID = " + this.candidateDetailsId + ";";

				int setRunStatusQueryResult = DBConnect.updateData(con, setRunStatusQuery);

				logger.info(setRunStatusQuery);

				if (setRunStatusQueryResult < 0) {

					throw new Exception("Unable to set Run status to Database for Candidate Details Id: "
							+ this.candidateDetailsId);
				}

				con.close();
				return this.testCaseRunStatus;

			} else {

				con.close();
				throw new Exception("No run module built for " + this.moduleName);
			}

		} catch (Exception e) {

			con.close();
			logger.error(e);
			throw new Exception(e.getMessage());
		}

	}

	public int exportToFile() throws Exception {

		String candidateAnswer = this.candidateEnteredAnswer.toString();

		candidateAnswer = candidateAnswer.replace(this.baseClassName, this.candidateCodeFileNameWithoutExtension);

		if (this.moduleName.equals("C")) {
			candidateAnswer = removeEscapeCharacters(candidateAnswer);
			candidateAnswer = "#include <stdio.h>\n#include <conio.h>\n#include <stdlib.h>\n#include <stdbool.h>\n#include <string.h>\n"
					+ candidateAnswer;
		} else if (this.moduleName.equals("C++")) {
			candidateAnswer = removeEscapeCharacters(candidateAnswer);
			candidateAnswer = "#include <iostream>\n#include <stdlib.h>\n#include <stdbool.h>\n#include <string.h>\nusing namespace std;"
					+ candidateAnswer;
		} else if (this.moduleName.equals("Java")) {
			candidateAnswer = removeEscapeCharacters(candidateAnswer);
		}

		logger.info(this.candidateFileLocation);

		Writer writer = null;

		try {

			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.candidateFileLocation)));

			writer.write(candidateAnswer);

			return 0;

		} catch (Exception e) {

			logger.error(e);
			return 1;

		} finally {

			try {

				writer.close();
			} catch (Exception ex) {

				logger.error(ex);
				return 1;

			}
		}
	}

	public static String removeEscapeCharacters(String answerToCorrect) {

		int contentLength = answerToCorrect.length();
		char[] answerInCharArray = answerToCorrect.toCharArray();
		ArrayList<Character> removedEscapeCharactersCharArray = new ArrayList<Character>();

		for (int i = 0; i < contentLength - 1; i++) {

			if (answerInCharArray[i] == '\\' && answerInCharArray[i + 1] == 't') {
				i++;
				continue;
			} else if (answerInCharArray[i] == '\\' && answerInCharArray[i + 1] == '"') {
				continue;
			}

			removedEscapeCharactersCharArray.add(answerInCharArray[i]);

		}

		removedEscapeCharactersCharArray.add(answerInCharArray[contentLength - 1]);
		StringBuilder sb = new StringBuilder();

		for (Character ch : removedEscapeCharactersCharArray) {
			sb.append(ch);
		}

		String answerWithoutEscapeCharacters = sb.toString();

		return answerWithoutEscapeCharacters;

	}

	public static String readProgramFromFile(String rawFileName) throws Exception {

		try {
			File file = new File(rawFileName);

			BufferedReader br = new BufferedReader(new FileReader(file));
			
			String fileContents = org.apache.commons.io.IOUtils.toString(br);
			
			logger.info("File Contents: " + fileContents);
			
			br.close();
			
			return fileContents;
			
		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}
	}
}
