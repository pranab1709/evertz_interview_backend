package evertz.evertz_interview_backend.application_api_calls;

/*
	Author: 	Sinjini
	Created Date: 	19th September 2019
	Modified Date: 	25th September 2019
	Description: 	Used to send proper JSON message once connection is successful.
					This class accept JSON Array as arguments and return JSON structure as string.
*/

import org.json.simple.JSONObject;
import java.sql.*;
import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;
import evertz.evertz_interview_backend.application_server.logger;

public class ValidateServer {

	private JSONObject validateServerParameterList;

	JSONObject reasonAndStatusObject = new JSONObject();

	public ValidateServer(JSONObject validateServerParameterList) {

		this.validateServerParameterList = validateServerParameterList;

	}

	@SuppressWarnings("unchecked")
	public JSONObject validateServer() throws Exception {

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		String serverIp = null;

		try {

			serverIp = validateAndGetServerIpFromParameters();

			boolean dbStatus = validateDBConnection(serverIp);

			if (dbStatus != true) {

				throw new Exception("DB connection failed");
			}

			responseParameterList.put("ServerIp", serverIp);
			responseParameterList.put("Status", "Connected");

			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);

			return returnResponseObject;

		} catch (Exception e) {

			responseParameterList.put("ServerIp", serverIp);
			responseParameterList.put("Status", e.getMessage());

			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);

			logger.error(e);
			return returnResponseObject;
		}
	}

	private boolean validateDBConnection(String serverIp) throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		try {

			String testConnectionQuery = "SELECT * FROM DEGREE";
			ResultSet queryResult = DBConnect.selectData(con, testConnectionQuery);

			if (queryResult.next() == false) {

				throw new Exception("DB connection failed");
			}

			con.close();
			return true;

		} catch (Exception e) {

			con.close();
			logger.error(e);
			throw new Exception("DB connection failed");
		}
	}

	private String validateAndGetServerIpFromParameters() throws Exception {

		try {
			if (this.validateServerParameterList == null) {

				throw new Exception("ParameterList from the request body is null. Need a valid ParameterList");
			}

			return ((String) this.validateServerParameterList.get("ServerIp").toString());
		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}
	}
}