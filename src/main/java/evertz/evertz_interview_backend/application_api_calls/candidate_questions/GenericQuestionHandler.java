package evertz.evertz_interview_backend.application_api_calls.candidate_questions;

import org.json.simple.JSONObject;
import evertz.evertz_interview_backend.application_api_calls.CodingQuestionHandler;
import evertz.evertz_interview_backend.application_api_calls.MCQQuestionHandler;

public class GenericQuestionHandler {
	
	private String candidateRegisterNumber = null;
	private String questionType = null;
	
	int selectedModule;

	public GenericQuestionHandler(JSONObject candidateDetailsJSON) throws Exception {
		
        this.questionType = (String) candidateDetailsJSON.get("QuestionType");
        this.candidateRegisterNumber = (String) candidateDetailsJSON.get("RegisterNumber");
        
        UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(this.candidateRegisterNumber);
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject generateQuestions() throws Exception {
		
		JSONObject responseObject = new JSONObject();
		JSONObject responseParameterList = new JSONObject();
		
		JSONObject responseFromHandler = null;
		
		if(this.questionType.equals("Coding")) {
			
			CodingQuestionHandler cqh = new CodingQuestionHandler(this.candidateRegisterNumber);
			responseFromHandler = cqh.generateQuestions();
			return responseFromHandler;
		}
		else if(this.questionType.equals("MCQ")) {
			
			MCQQuestionHandler mqh = new MCQQuestionHandler(this.candidateRegisterNumber);
			responseFromHandler = mqh.generateQuestions();
			return responseFromHandler;
		}
		
		responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
		responseObject.put("Success", false);
		responseObject.put("Reason", "Invalid QuestionType: " + this.questionType);
		responseObject.put("ParameterList", responseParameterList);
		
		return responseObject;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getQuestions() throws Exception {
		
		JSONObject responseObject = new JSONObject();
		JSONObject responseParameterList = new JSONObject();
		
		JSONObject responseFromHandler = null;
		
		if(this.questionType.equals("Coding")) {
			
			CodingQuestionHandler cqh = new CodingQuestionHandler(this.candidateRegisterNumber);
			responseFromHandler = cqh.getQuestions();
			return responseFromHandler;
		}
		else if(this.questionType.equals("MCQ")) {
			
			MCQQuestionHandler mqh = new MCQQuestionHandler(this.candidateRegisterNumber);
			responseFromHandler = mqh.getQuestions();
			return responseFromHandler;
		}
		
		responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
		responseObject.put("Success", false);
		responseObject.put("Reason", "Invalid QuestionType: " + this.questionType);
		responseObject.put("ParameterList", responseParameterList);
		
		return responseObject;
	}
}

