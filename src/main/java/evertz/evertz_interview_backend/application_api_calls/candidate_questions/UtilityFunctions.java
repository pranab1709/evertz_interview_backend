/*
	Author: 		Shreelaxmi
	Created Date: 	24th September 2019
	Modified Date: 	3rd October 2019
	Description: 	Functions to validate data received from UI during registration of a candidate.
 */

package evertz.evertz_interview_backend.application_api_calls.candidate_questions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mysql.cj.jdbc.DatabaseMetaData;

import evertz.evertz_interview_backend.application_db_calls.ConnectToDatabase;

public class UtilityFunctions {

	static String regex = "";

	public static int getColumnSizeFromDatabase(String tableName, String column) throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		int columnSize = 0;
		ResultSet rsColumns = null;
		DatabaseMetaData meta = (DatabaseMetaData) con.getMetaData();

		rsColumns = meta.getColumns(null, null, tableName, null);
		while (rsColumns.next()) {
			String columnName = rsColumns.getString("COLUMN_NAME");
			int size = rsColumns.getInt("COLUMN_SIZE");
			if (columnName.equals(column)) {
				columnSize = size;
			}
		}
		return columnSize;

	}

	public  String validateEmailId(String email) throws Exception {


		int emailLengthAllocated = UtilityFunctions.getColumnSizeFromDatabase("CANDIDATE_DETAILS", "EMAIL");
		regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

		if (email.length() > emailLengthAllocated) {
			throw new Exception("Email id's length exceeds the limit. Only 100 characters allowed");
		}
		if (!email.contains("@")) {
			throw new Exception("Invalid Email ID format : missing @ symbol.");
		}
		if (!email.contains(".")) {
			throw new Exception("Invalid Email ID format : missing '.' symbol.");
		}
		if (!email.matches(regex)) {
			throw new Exception("Invalid Email ID format.");
		}
		return "True";
	}

	public  String validateMobileNumber(String phoneNumber) throws Exception {

		if (phoneNumber.length() != 10) {
			throw new Exception("Phone number length mismatch : Only 10 digits allowed. ");
		}
		if (!phoneNumber.matches("^[0-9]{10}$")) {
			throw new Exception(
					"Invalid Mobile number : The number cannot contain alphabets");
		}
		return "True";
	}

	public  String validateCgpa(String cgpa) throws Exception {

		regex = "^([0-9]{0,2}+(\\.[0-9]{0,2}))?$";
		if (!cgpa.matches(regex)) {
			throw new Exception("Invalid CGPA format : Only two numbers allowed before and after decimal.");
		}
		return "True";
	}

	public  String validateGraduationYear(String graduatingYear) throws Exception {

		if (graduatingYear.length() != 4) {
			throw new Exception("Invalid graduating year : Only four numbers allowed.");
		}
		if (!graduatingYear.matches("^[0-9]{4}$")) {
			throw new Exception("Invalid graduating year : Only numbers are allowed.");
		}
		return "True";
	}

	public  String validateRegisterNumber(String registrationNumber) throws Exception {

		int registerNumberLengthAllocated = UtilityFunctions.getColumnSizeFromDatabase("CANDIDATE_DETAILS", "REG_NO");
		int lengthOfRegisterNumber = registrationNumber.length();

		if (lengthOfRegisterNumber > registerNumberLengthAllocated) {
			throw new Exception("Length of Register number exceeds the limit. Only 20 characters allowed");
		}
		if (!registrationNumber.toUpperCase().equals(registrationNumber)) {
			throw new Exception("Register number should be in uppercase.");
		}
		if (!registrationNumber.matches("^[0-9a-zA-Z]+$")) {
			throw new Exception("No special characters are allowed in Register number.");
		}
		return "True";

	}

	public  String validateCandidatetName(String name) throws Exception {

		int candidateNameLengthAllocated = UtilityFunctions.getColumnSizeFromDatabase("CANDIDATE_DETAILS", "NAME");
		regex = "^[a-zA-Z0-9 ]+$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(name);
		int lengthOfCandidateName = name.length();

		if (lengthOfCandidateName > candidateNameLengthAllocated) {
			throw new Exception("Length of candidate name exceeds the limit.");
		}
		if (!matcher.matches()) {
			throw new Exception("Candidate name cannot have special characters.");
		}
		return "True";
	}

	public  String validateDateOfBirth(String dateOfBirth) throws Exception {

		String regex = "(^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[1][0-9]|[2][0-9]|0[1-9])$)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher((CharSequence) dateOfBirth);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		Date birthDate = sdf.parse(dateOfBirth);
		Instant instant = birthDate.toInstant();
		ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
		LocalDate givenDate = zone.toLocalDate();
		Period period = Period.between(givenDate, LocalDate.now());
		int currentAge = period.getYears();

		if (currentAge < 20) {
			throw new Exception("Age less than 20, not permitted to write the test.");
		}
		if (!matcher.matches()) {
			throw new Exception("Invalid Date of Birth : Date format allowed is yyyy-mm-dd.");
		}
		return "True";
	}

}