
package evertz.evertz_interview_backend.application_api_calls.candidate_response;

/*

	Author: 		Balaji M S
	Created Date: 	20th September 2019
	Modified Date: 	22nd September 2019
	Description: 	This is a generic class that handles the candidate responses, either the MCQs or the Coding questions 

*/

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import evertz.evertz_interview_backend.application_api_calls.SaveOrSubmitCodingAnswer;
import evertz.evertz_interview_backend.application_api_calls.SaveOrSubmitMCQAnswer;
import evertz.evertz_interview_backend.application_api_calls.ValidateCodingAnswer;
import evertz.evertz_interview_backend.application_api_calls.candidate_questions.UtilityFunctions;
import evertz.evertz_interview_backend.application_server.logger;

public class CandidateResponseDispatcher {

	public JSONObject parameterList;

	public String responseOption = null;;
	public String questionType = null;
	public String candidateRegisterNumber = null;
	public String questionId = null;
	public String candidateEnteredAnswer = null;
	public JSONObject questionAndAnswerDetails = null;
	public String[] testCaseResult;

	public CandidateResponseDispatcher(JSONObject parameterList, String responseOption) throws Exception {

		this.parameterList = parameterList;
		this.responseOption = responseOption;
		this.questionType = (String) parameterList.get("QuestionType");
		this.candidateEnteredAnswer = (String) parameterList.get("Answer");
		this.candidateRegisterNumber = (String) parameterList.get("RegisterNumber");
		this.questionId = (String) parameterList.get("QuestionId").toString();

		UtilityFunctions utilityFunction = new UtilityFunctions();
		utilityFunction.validateRegisterNumber(candidateRegisterNumber);
	}

	@SuppressWarnings("unchecked")
	public JSONObject sendToResponseHandler() throws Exception {

		JSONObject evertzinterviewappObj = new JSONObject();

		evertzinterviewappObj.put("Subsystem", "Candidate");
		evertzinterviewappObj.put("Command", this.responseOption);

		JSONObject parameterList_obj = new JSONObject();
		parameterList_obj.put("RegisterNumber", this.candidateRegisterNumber);
		parameterList_obj.put("QuestionId", this.questionId);
		parameterList_obj.put("QuestionType", this.questionType);

		if (this.questionType.equals("Coding")) {

			JSONObject responseParameterList = new JSONObject();
			JSONObject returnResponseObject = new JSONObject();

			SaveOrSubmitCodingAnswer sca = new SaveOrSubmitCodingAnswer(this.candidateRegisterNumber, this.questionId,
					this.candidateEnteredAnswer);

			if (this.responseOption.equals("SaveAnswer")) {

				try {

					int saveResult = sca.saveAnswerToFile();

					if (saveResult == 0) {

						logger.info("Answer saved for QuestionId: " + this.questionId + " for Register Number: "
								+ this.candidateRegisterNumber);

					} else {

						throw new Exception("Unable to Save Answer for QuestionId: " + this.questionId
								+ " for Register Number: " + this.candidateRegisterNumber);
					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}

			} else if (this.responseOption.equals("SubmitAnswer")) {

				try {

					int submitResult = sca.submitAnswer();

					if (submitResult == 0) {

						logger.info("Coding answer submitted for QuestionId: " + this.questionId
								+ " for Register Number: " + this.candidateRegisterNumber);

					} else {

						throw new Exception("Unable to submit coding answer for QuestionId: " + this.questionId
								+ " for Register Number: " + this.candidateRegisterNumber);
					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}

			} else if (this.responseOption.equals("GetSavedProgram")) {
				try {

					String programData = sca.getSavedProgramFromFile();

					if (programData == null || programData.isEmpty()) {

						throw new Exception("Unable to get coding answer for QuestionId: " + this.questionId
								+ " for Register Number: " + this.candidateRegisterNumber);

					} else {

						logger.info("Coding part successfully fetched from DB.");
					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);
					responseParameterList.put("Program", programData);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}
			}

			else if (this.responseOption.equals("CompileProgram")) {

				try {

					ValidateCodingAnswer va = new ValidateCodingAnswer(this.candidateRegisterNumber, this.questionId);
					int compilationStatus = va.compileProgram();

					if (compilationStatus != 1) {

						throw new Exception(
								"Unable to compile program for Regiser Number: " + this.candidateRegisterNumber);

					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}
			}

			else if (this.responseOption.equals("RunProgram")) {

				try {

					JSONArray dataElementObj = new JSONArray();

					ValidateCodingAnswer va = new ValidateCodingAnswer(this.candidateRegisterNumber, this.questionId);

					int numberOfTestCases = va.getNumberOfTestCases();

					String[] runStatusArray = new String[numberOfTestCases];
					String[] testCases = new String[numberOfTestCases];
					String[] testCaseExpectedOutput = new String[numberOfTestCases];
					String[] testCaseHidden = new String[numberOfTestCases];

					testCases = va.getTestCaseArray();
					testCaseExpectedOutput = va.getTestCaseOutput();
					testCaseHidden = va.getTestCaseHiddenStatus();

					runStatusArray = va.runProgram();

					for (int i = 0; i < numberOfTestCases; i++) {

						JSONObject eachData = new JSONObject();
						eachData.put("TestCaseId", i + 1);
						eachData.put("TestCase", testCases[i]);
						eachData.put("TestCaseHiddenStatus", testCaseHidden[i]);

						if (!testCaseHidden[i].equals("1")) {

							eachData.put("TestCaseOutput", runStatusArray[i]);
							eachData.put("TestCaseExpectedOutput", testCaseExpectedOutput[i]);
						}

						if (testCaseExpectedOutput[i].equals(runStatusArray[i])) {

							eachData.put("TestCaseResultStatus", "0");
						} else {

							eachData.put("TestCaseResultStatus", "1");
						}

						dataElementObj.add(eachData);

					}

					responseParameterList.put("TestCaseList", dataElementObj);
					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);
					responseParameterList.put("QuestionType", "Coding");

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}
			}

		} else if (this.questionType.equals("MCQ")) {

			JSONObject responseParameterList = new JSONObject();
			JSONObject returnResponseObject = new JSONObject();

			SaveOrSubmitMCQAnswer sma = new SaveOrSubmitMCQAnswer(this.candidateRegisterNumber, this.questionId,
					this.candidateEnteredAnswer);

			if (this.responseOption.equals("SaveAnswer")) {

				try {

					int saveResult = sma.saveAnswer();

					if (saveResult == 0) {

						logger.info("Answer saved for QuestionId: " + this.questionId + " for Register Number: "
								+ this.candidateRegisterNumber);

					} else {

						throw new Exception("Unable to Save Answer for QuestionId: " + this.questionId
								+ " for Register Number: " + this.candidateRegisterNumber);
					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}

			}

			else if (this.responseOption.equals("SubmitAnswer")) {

				try {

					int submitResult = sma.submitAnswer();

					if (submitResult == 0) {

						logger.info("Session Submitted for Register Number: " + this.candidateRegisterNumber);

					} else {

						throw new Exception(
								"Unable to Submit Session for Register Number: " + this.candidateRegisterNumber);
					}

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);
					responseParameterList.put("QuestionId", this.questionId);

					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);

					return returnResponseObject;

				} catch (Exception e) {

					responseParameterList.put("RegisterNumber", this.candidateRegisterNumber);

					returnResponseObject.put("Success", false);
					returnResponseObject.put("Reason", e.getMessage());
					returnResponseObject.put("ParameterList", responseParameterList);

					logger.error(e);
					return returnResponseObject;

				}
			}
		}
		return null;
	}

}
