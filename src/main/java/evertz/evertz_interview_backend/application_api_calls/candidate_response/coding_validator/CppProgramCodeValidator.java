package evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import evertz.evertz_interview_backend.application_server.SystemConfig;
import evertz.evertz_interview_backend.application_server.logger;

public class CppProgramCodeValidator {

	public String candidateCodeFileLocation = null;
	public String candidateProgramLocation = null;
	public String candidateCodeFileNameWithoutExtension = null;
	public String cppProgramCompiler = null;

	public CppProgramCodeValidator(String candidateProgramLocation, String candidateCodeFileLocation,
			String candidateCodeFileNameWithoutExtension) throws Exception {

		this.candidateCodeFileLocation = candidateCodeFileLocation;
		this.candidateProgramLocation = candidateProgramLocation;
		this.candidateCodeFileNameWithoutExtension = candidateCodeFileNameWithoutExtension;
		this.cppProgramCompiler = SystemConfig.getSystemConfig("CppProgramCompiler");

	}

	public int compileProgram() throws Exception {
		
		try {
			
			logger.info(this.cppProgramCompiler + " -o " + this.candidateProgramLocation + this.candidateCodeFileNameWithoutExtension + " " + this.candidateCodeFileLocation);
			Process compilationProcess = Runtime.getRuntime().exec(
					this.cppProgramCompiler + " -o " + this.candidateProgramLocation + this.candidateCodeFileNameWithoutExtension + " " + this.candidateCodeFileLocation);

			String programErrorOutput = handleOutputAndExceptions(compilationProcess);

			if (programErrorOutput.isEmpty()) {

				return 0;
			}

			throw new Exception(programErrorOutput);

		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}
	}

	public String runProgram(String testCase, String testCaseAnswer) throws Exception {

		try {

			Process proc = Runtime.getRuntime().exec(
					this.candidateProgramLocation + this.candidateCodeFileNameWithoutExtension + " \"" + testCase + "\"");

			String programOutput = handleOutputAndExceptions(proc);

			return programOutput;

		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}

	}

	public String handleOutputAndExceptions(Process proc) throws IOException {

		InputStream errorOut = proc.getErrorStream();
		InputStream in = proc.getInputStream();

		BufferedReader errorOutput = new BufferedReader(new InputStreamReader(errorOut));
		BufferedReader output = new BufferedReader(new InputStreamReader(in));

		String programErrorOutput = errorOutput.lines().collect(Collectors.joining("\n"));

		String programOutput = org.apache.commons.io.IOUtils.toString(output);

		errorOutput.close();
		output.close();

		if (programErrorOutput.isEmpty()) {

			return programOutput;
		}

		return programErrorOutput + "\n" + programOutput;

	}

}
