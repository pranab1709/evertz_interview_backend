package evertz.evertz_interview_backend.application_api_calls.candidate_response.coding_validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import evertz.evertz_interview_backend.application_server.SystemConfig;

public class JavaCodeValidator {

	public String candidateCodeFileLocation = null;
	public String candidateProgramLocation = null;
	public String candidateCodeFileNameWithoutExtension = null;
	public String javaProgramCompiler = null;
	public String javaProgramExecutor = null;

	public JavaCodeValidator(String candidateProgramLocation, String candidateCodeFileLocation,
			String candidateCodeFileNameWithoutExtension) throws Exception {

		this.candidateCodeFileLocation = candidateCodeFileLocation;
		this.candidateProgramLocation = candidateProgramLocation;
		this.candidateCodeFileNameWithoutExtension = candidateCodeFileNameWithoutExtension;
		this.javaProgramCompiler = SystemConfig.getSystemConfig("JavaProgramCompiler");
		this.javaProgramExecutor = SystemConfig.getSystemConfig("JavaProgramExecutor");

	}

	public int compileProgram() throws Exception {

		try {

			Process compilationProcess = Runtime.getRuntime().exec(this.javaProgramCompiler + " -cp . " + this.candidateCodeFileLocation);

			String programErrorOutput = handleOutputAndExceptions(compilationProcess);

			if (programErrorOutput.isEmpty()) {

				return 0;
			}

			throw new Exception(programErrorOutput);

		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}
	}

	public String runProgram(String testCase, String testCaseAnswer) throws Exception {

		try {
			Process proc = Runtime.getRuntime().exec("java -cp " + this.candidateProgramLocation + " "
					+ this.candidateCodeFileNameWithoutExtension + " \"" + testCase + "\"");

			String programOutput = handleOutputAndExceptions(proc);

			return programOutput;

		} catch (Exception e) {

			throw new Exception(e.getMessage());
		}

	}

	public String handleOutputAndExceptions(Process proc) throws IOException {

		InputStream errorOut = proc.getErrorStream();
		InputStream in = proc.getInputStream();

		BufferedReader errorOutput = new BufferedReader(new InputStreamReader(errorOut));
		BufferedReader output = new BufferedReader(new InputStreamReader(in));

		String programErrorOutput = errorOutput.lines().collect(Collectors.joining("\n"));
		
		String programOutput = org.apache.commons.io.IOUtils.toString(output).replaceAll("\r\n", "<br>");

		errorOutput.close();
		output.close();

		if (programErrorOutput.isEmpty()) {

			return programOutput;
		}

		return programErrorOutput + "\n" + programOutput;

	}

}
