package evertz.evertz_interview_backend.application_server;

/*
    Author: 		Shreelakshmi
    Created Date: 	10th September 2019
    Modified Date: 	19th September 2019
    Description: 	This class would just get the Request from the ApplicationServer class and pass it on to the right Api Call classes 

*/

import org.json.simple.JSONObject;

import evertz.evertz_interview_backend.application_api_calls.Admin;
import evertz.evertz_interview_backend.application_api_calls.Candidate;
import evertz.evertz_interview_backend.application_api_calls.GenerateResults;
import evertz.evertz_interview_backend.application_api_calls.UI;
import evertz.evertz_interview_backend.application_api_calls.ValidateServer;
import evertz.evertz_interview_backend.application_api_calls.candidate_questions.GenericQuestionHandler;
import evertz.evertz_interview_backend.application_api_calls.candidate_response.CandidateResponseDispatcher;

public class DispatchToSubsystem {

	String subsystemName = null;
	String commandName = null;
	JSONObject parameterList = null;

	public DispatchToSubsystem(JSONObject requestFromClient) {

		this.subsystemName = (String) requestFromClient.get("Subsystem");
		this.commandName = (String) requestFromClient.get("Command");

		this.parameterList = (JSONObject) requestFromClient.get("ParameterList");

	}

	@SuppressWarnings("unchecked")
	public JSONObject sendToRightHandler() throws Exception {

		// JSON parser object to parse read the response
		JSONObject returnFromHandler = new JSONObject();
		JSONObject responseObject = new JSONObject();

		try {

			if ((this.subsystemName.equals("Server")) && (this.commandName.equals("ValidateServer"))) {

				ValidateServer validate = new ValidateServer(parameterList);
				returnFromHandler = validate.validateServer();

			} else if ((this.subsystemName.equals("UI")) && (this.commandName.equals("Get"))) {

				UI ui = new UI(parameterList);
				returnFromHandler = ui.getJsonList();

			} else if ((this.subsystemName.equals("UI")) && (this.commandName.equals("Add"))) {

				UI ui = new UI(parameterList);
				returnFromHandler = ui.addNewUIData();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("Save"))) {

				Candidate candidate = new Candidate(parameterList);
				returnFromHandler = candidate.register();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("TestStatus"))) {

				Candidate candidate = new Candidate(parameterList);
				returnFromHandler = candidate.testTakenStatus();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("GenerateQuestions"))) {

				GenericQuestionHandler gqh = new GenericQuestionHandler(parameterList);
				returnFromHandler = gqh.generateQuestions();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("GetQuestions"))) {

				GenericQuestionHandler gqh = new GenericQuestionHandler(parameterList);
				returnFromHandler = gqh.getQuestions();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("SaveAnswer"))) {

				CandidateResponseDispatcher crd = new CandidateResponseDispatcher(parameterList, "SaveAnswer");
				returnFromHandler = crd.sendToResponseHandler();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("SubmitAnswer"))) {

				CandidateResponseDispatcher crd = new CandidateResponseDispatcher(parameterList, "SubmitAnswer");
				returnFromHandler = crd.sendToResponseHandler();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("CompileProgram"))) {

				CandidateResponseDispatcher crd = new CandidateResponseDispatcher(parameterList, "CompileProgram");
				returnFromHandler = crd.sendToResponseHandler();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("RunProgram"))) {

				CandidateResponseDispatcher crd = new CandidateResponseDispatcher(parameterList, "RunProgram");
				returnFromHandler = crd.sendToResponseHandler();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("GetSavedProgram"))) {

				CandidateResponseDispatcher crd = new CandidateResponseDispatcher(parameterList, "GetSavedProgram");
				returnFromHandler = crd.sendToResponseHandler();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("Login"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.Login();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("AddUser"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.AddUser();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("UpdateUser"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.UpdateUser();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("AddQuestion"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.addQuestion();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("GenerateResult"))) {

				GenerateResults a = new GenerateResults(parameterList);
				returnFromHandler = a.getJsonList();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("GetMandatoryModules"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.getMandatoryModules();

			} else if ((this.subsystemName.equals("Candidate")) && (this.commandName.equals("ResetTest"))) {

				Candidate candidate = new Candidate(parameterList);
				returnFromHandler = candidate.resetCandidateData();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("GetAll"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.getAllStatus();

			} else if ((this.subsystemName.equals("Admin")) && (this.commandName.equals("SetCollege"))) {

				Admin a = new Admin(parameterList);
				returnFromHandler = a.setCollegeStatus();

			} else {

				throw new Exception("Requested API not found or not built");

			}

			returnFromHandler.put("Subsystem", this.subsystemName);
			returnFromHandler.put("Command", this.commandName);
			responseObject.put("EvertzInterviewApp", returnFromHandler);

			return responseObject;

		} catch (Exception e) {

			logger.error(e);

			returnFromHandler.put("Success", false);
			returnFromHandler.put("Reason", e.getMessage());

			returnFromHandler.put("Subsystem", this.subsystemName);
			returnFromHandler.put("Command", this.commandName);

			responseObject.put("EvertzInterviewApp", returnFromHandler);

			return responseObject;
		}
	}
}