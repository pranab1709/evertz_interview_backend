package evertz.evertz_interview_backend.application_server;

/*
	Author: 		Balaji
	Created Date: 	10th September 2019
	Modified Date: 	27th September 2019
	Description: 	This is the start of the application. All the requests from Frontend or from other applications is sent to the right classes from here 

*/

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.stream.Collectors;

import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import evertz.evertz_interview_backend.application_server.logger;

public class EvertzInterviewAppServer {

	public static String returnMessage;
	public static String portToListen;
	public static String apiEndPoint;

	public static JSONObject responseObject = new JSONObject();
	public static JSONObject reasonAndStatusObject = new JSONObject();

	/*
	 * This method will be the first method to start after the program is run. This
	 * starts the server based on the port information from the SystemConfig file
	 */

	public static void main(String args[]) {

		// Getting the Port and End point information from the SystemConfig file
		try {

			setLogProperties();

			portToListen = SystemConfig.getSystemConfig("ServerListeningPort");
			apiEndPoint = SystemConfig.getSystemConfig("APIEndPoint");
		} catch (Exception e) {

			logger.error(e);
			System.exit(1);
		}

		System.out.println("Update system config in this file: " + SystemConfig.systemConfigLocation);
		System.out.println("------------------------------------------");
		System.out.println("Server listening to port: " + portToListen);
		System.out.println("------------------------------------------");
		System.out.println("Accepted Endpoint: /" + apiEndPoint);
		System.out.println("------------------------------------------");

		// Start the server
		try {
			HttpServer server = HttpServer.create(new InetSocketAddress(Integer.parseInt(portToListen)), 0);
			HttpContext context = server.createContext("/" + apiEndPoint);
			context.setHandler(EvertzInterviewAppServer::handleRequest);
			server.start();

		} catch (Exception e) {

			logger.error(e);
			System.exit(1);
		}
	}

	/*
	 * This function sends the response back to the client
	 */

	@SuppressWarnings({ "unchecked" })
	private static void handleRequest(HttpExchange exchange) throws IOException {

		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
		exchange.getResponseHeaders().add("Access-Control-Allow-Headers",
				"X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, X-Auth-Token, content-type, Authentication");

		try {

			printRequestInfo(exchange);

		} catch (Exception e) {

			reasonAndStatusObject.put("Success", false);
			reasonAndStatusObject.put("Reason", e.getMessage());
			responseObject.put("EvertzInterviewApp", reasonAndStatusObject);

			returnMessage = responseObject.toString();
		}
		
		logger.info("Response: " + returnMessage);
		exchange.sendResponseHeaders(200, returnMessage.getBytes().length);
		OutputStream os = exchange.getResponseBody();
		os.write(returnMessage.getBytes());
		os.close();
	}

	/*
	 * This function sends the request to the right class
	 */

	private static void printRequestInfo(HttpExchange exchange) throws Exception {

		try {

			InputStreamReader isr;
			isr = new InputStreamReader(exchange.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String requestBodyString = br.lines().collect(Collectors.joining());

			if (requestBodyString.isEmpty()) {

				logger.info("Empty request received. Nothing to do.");
				
			} else {
				
				logger.info("Request: " + requestBodyString);
				
				JSONParser jsonParser = new JSONParser();

				JSONObject jsonRequestMessageObject = (JSONObject) jsonParser.parse(requestBodyString);

				JSONObject evertzInterviewApp = (JSONObject) jsonRequestMessageObject.get("EvertzInterviewApp");

				String subsystemName = (String) evertzInterviewApp.get("Subsystem");
				String commandName = (String) evertzInterviewApp.get("Command");

				JSONObject parameterList = (JSONObject) evertzInterviewApp.get("ParameterList");

				// Subsystem, CommandName and ParameterList is mandatory for each APICall.
				// Validating and returning
				boolean validationResult = validateAPICall(subsystemName, commandName, parameterList);

				if (validationResult != true) {

					throw new Exception("Unknown error occurred");
				}

				DispatchToSubsystem dts = new DispatchToSubsystem(evertzInterviewApp);
				JSONObject responseFromSubsystem = dts.sendToRightHandler();

				returnMessage = responseFromSubsystem.toString();
			}

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}

	/*
	 * This function validates to check if the three mandatory fields and values are
	 * available
	 */
	private static boolean validateAPICall(String subsystemName, String commandName, JSONObject parameterList)
			throws Exception {

		if (subsystemName.isEmpty() || subsystemName == null) {

			throw new Exception("Subsystem is empty or not provided");

		} else if (commandName.isEmpty() || commandName == null) {

			throw new Exception("CommandName is empty or not provided");

		} else if (parameterList.isEmpty() || parameterList == null) {

			throw new Exception("ParameterList is empty or not provided");

		}

		return true;

	}

	private static void setLogProperties() {

		try {

			String operatingSystem = SystemConfig.getSystemConfig("OperatingSystem");

			System.setProperty("logFileLoc", SystemConfig.getSystemConfig("LogFileLocation" + operatingSystem));

			String log4jConfPath = SystemConfig.getSystemConfig("Log4jPropertiesLocation");

			PropertyConfigurator.configure(log4jConfPath);

			String candidateProgramLocation = SystemConfig.getSystemConfig("CandidateCodeLocation" + operatingSystem);

			File directory = new File(candidateProgramLocation);

			if (!directory.exists()) {

				directory.mkdir();
			}

		} catch (Exception e) {

			System.out.println("Unable to load logger properties. " + e.getMessage());
		}
	}
}
