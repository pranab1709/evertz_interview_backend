package evertz.evertz_interview_backend.application_server;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.*;

public class logger {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(logger.class);	
	
	public static void error(Exception message) {
		System.out.println("Error: " + ExceptionUtils.getStackTrace(message));
		log.error(ExceptionUtils.getStackTrace(message));
	}
	
	public static void info(Exception message) {
		System.out.println("Info: " + ExceptionUtils.getStackTrace(message));
		log.info(ExceptionUtils.getStackTrace(message));
	}
	
	public static void error(String message) {
		System.out.println("Error: " + message);
		log.error(message);
	}
	
	public static void info(String message) {
		System.out.println("Info: " + message);
		log.info(message);
	}
	
}
